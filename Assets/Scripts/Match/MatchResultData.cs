﻿namespace Assets.Scripts.Match
{
    internal struct MatchResultData
    {
        public MatchResultType MatchResult;
        public string ResultText;
        //public bool IsMatchSeriesOver;
    }
}

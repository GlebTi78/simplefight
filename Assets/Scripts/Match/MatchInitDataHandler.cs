﻿using Assets.Scripts.Save;
using Assets.Scripts.Unit.Characters;
using UnityEngine;

namespace Assets.Scripts.Match
{
    internal class MatchInitDataHandler
    {
        private BaseCharacter character;
        private byte maxId = 4;
        private int characterId;
        private int botId;

        public MatchInitDataHandler()
        {
            characterId = PlayerPrefs.GetInt(PlayerPrefsKeys.CharacterId);
            //Debug.Log($"[{this.GetType().Name}] стартоввый CharacterId = {characterId}");
            switch (characterId)
            {
                case 1:
                    character = new Character_1();
                    break;
                case 2:
                    character = new Character_2();
                    break;
                case 3:
                    character = new Character_3();
                    break;
                case 4:
                    character = new Character_4();
                    break;
                default:
                    Debug.Log($"[{this.GetType().Name}] ошибка идентификации персонажа");
                    characterId = 1;
                    character = new Character_1();
                    break;
            }
        }

        public BaseCharacter GetCharacter() => character;

        public BaseCharacter GetBot()
        {
            IdValidation();
            var bot = Selection();
            SaveNewBotId();
            return bot;
        }

        private void IdValidation()
        {
            botId = PlayerPrefs.GetInt(PlayerPrefsKeys.BotId);

            if (botId < 1 || botId > maxId)
            {
                //Debug.Log($"[{this.GetType().Name}] bot id = {botId}");
                //Debug.Log($"[{this.GetType().Name}] коррестировка bot id");
                botId = 1;
            }

            if (characterId == botId)
            {
                //Debug.Log($"[{this.GetType().Name}] bot id = character id");
                //Debug.Log($"[{this.GetType().Name}] коррестировка bot id");
                botId++;
            }

            if (botId < 1 || botId > maxId)
            {
                //Debug.Log($"[{this.GetType().Name}] bot id = {botId}");
                //Debug.Log($"[{this.GetType().Name}] коррестировка bot id");
                botId = 1;
            }

            //Debug.Log($"[{this.GetType().Name}] Id validation:");
            //Debug.Log($"[{this.GetType().Name}] character id = {characterId}");
            //Debug.Log($"[{this.GetType().Name}] bot id = {botId}");
        }

        private BaseCharacter Selection()
        {
            switch (botId)
            {
                case 1:
                    return new Character_1();
                case 2:
                    return new Character_2();
                case 3:
                    return new Character_3();
                case 4:
                    return new Character_4();
                default:
                    Debug.LogError($"[{this.GetType().Name}] ошибка определения соперника, определен соперник по умолчанию");
                    return new Character_1();
            }
        }

        private void SaveNewBotId()
        {
            botId++;
            PlayerPrefs.SetInt(PlayerPrefsKeys.BotId, botId);
        }
    }
}


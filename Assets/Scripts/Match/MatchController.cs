﻿using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.MatchSeries;
using Assets.Scripts.Save;
using Assets.Scripts.UI.GameScene;
using Assets.Scripts.Unit;
using Assets.Scripts.Unit.Characters;
using System;
using UnityEngine;

namespace Assets.Scripts.Match
{
    internal class MatchController : IController
    {
        private DIContainer container;
        private ScoreController scoreController;
        private BaseCharacter character;
        private BaseCharacter bot;
        private MatchSeriesController matchSeriesController;

        public BaseCharacter Character => character;
        public BaseCharacter Bot => bot;

        public MatchController(DIContainer container)
        {
            this.container = container;

            var matchData = new MatchInitDataHandler();
            character = matchData.GetCharacter();
            bot = matchData.GetBot();
        }

        public void Init()
        {
            scoreController = container.Get<ScoreController>();
            matchSeriesController = container.Get<MatchSeriesController>();
        }

        public void MatchCompleted()
        {
            MatchScore score = scoreController.GetScore();

            MatchResultType matchResult;
            string resultText;

            if (score.PlayerScore == score.BotScore)
            {
                matchResult = MatchResultType.Draw;
                resultText = $"Матч окончен вничью со счетом\n{score.PlayerScore} : {score.BotScore}";
            }
            else if (score.PlayerScore > score.BotScore)
            {
                matchResult = MatchResultType.Win;
                resultText = $"Player победил со счетом\n{score.PlayerScore} : {score.BotScore}";
            }
            else
            {
                matchResult = MatchResultType.Defeat;
                resultText = $"Player проиграл со счетом\n{score.PlayerScore} : {score.BotScore}";
            }

            matchSeriesController.ProcessMatchResults(matchResult, resultText);
        }

        public void KnockoutEvent(UnitManager winner)
        {
            MatchResultType matchResult;
            string resultText;

            if (winner == UnitManager.Player)
                matchResult = MatchResultType.Win;
            else
                matchResult = MatchResultType.Defeat;

            resultText = $"{winner} победил нокаутом.";

            matchSeriesController.ProcessMatchResults(matchResult, resultText, true);
        }

        public void NotEnoughInitiative(UnitManager winner)
        {
            MatchResultType matchResult;
            string resultText;

            if (winner == UnitManager.None)
            {
                matchResult = MatchResultType.Draw;
                resultText = $"Ничья\nОба соперника утратили инициативу.";
            }
            else if (winner == UnitManager.Player)
            {
                matchResult = MatchResultType.Win;
                resultText = $"Player победил!\nСоперник утратил инициативу и признал поражение в матче.";
            }

            else
            {
                matchResult = MatchResultType.Defeat;
                resultText = $"Player утратил инициативу и признал поражение в матче.";
            }

            matchSeriesController.ProcessMatchResults(matchResult, resultText);
        }
    }
}

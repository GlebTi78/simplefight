﻿namespace Assets.Scripts.Match
{
    internal enum MatchResultType
    {
        None,
        Win,
        Defeat,
        Draw
    }
}

﻿using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.MatchSeries;
using Assets.Scripts.Save;
using UnityEngine;

namespace Assets.Scripts.Match
{
    internal class MatchSettingsHolder
    {
        public bool IsTillDropSeries()
        {
            int result = PlayerPrefs.GetInt(PlayerPrefsKeys.CompetitionType);
            
            if (result == (int)CompetitionType.TillDrop)
                return true;
            else
                return false;
        }
        
        public int GetPlayerIni()
        {
            int initiative = Resources.Load<MatchSettings>("MatchSettings").PlayerInitiativeStartValue;

            if (IsStandartMatchType())
            {
                return initiative;
            }

            if (PlayerPrefs.GetInt(PlayerPrefsKeys.IsStartSeries) == 1)
            {
                return initiative;
            }
            else
            {
                initiative = PlayerPrefs.GetInt(PlayerPrefsKeys.PlayerInitiative);
                byte increase = Resources.Load<MatchSettings>("MatchSettings").InitiativeIncreased;
                initiative += increase;
                return initiative;
            }

        }

        public void SaveIntermediateResult(MatchResultType matchResult, int rounds, int playerIni)
        {
            int winsNumber = PlayerPrefs.GetInt(PlayerPrefsKeys.WinsNumber);

            if (matchResult == MatchResultType.Win)
            {
                winsNumber++;
                PlayerPrefs.SetInt(PlayerPrefsKeys.WinsNumber, winsNumber);
            }

            var roundsNumber = PlayerPrefs.GetInt(PlayerPrefsKeys.RoundsNumber);
            roundsNumber += rounds - 1;
            
            PlayerPrefs.SetInt(PlayerPrefsKeys.RoundsNumber, roundsNumber);
            PlayerPrefs.SetInt(PlayerPrefsKeys.PlayerInitiative, playerIni);
            PlayerPrefs.SetInt(PlayerPrefsKeys.IsStartSeries, 0);

            Debug.Log($"[{this.GetType().Name}] Победы: {winsNumber}");
            Debug.Log($"[{this.GetType().Name}] Раунды: {roundsNumber}");
        }

        private bool IsStandartMatchType()
        {
            var competitionTypeIndex = PlayerPrefs.GetInt(PlayerPrefsKeys.CompetitionType);
            if (competitionTypeIndex == (int)CompetitionType.Standard)
                return true;
            else 
                return false;
        }
    }
}

﻿namespace Assets.Scripts.Match
{
    internal struct MatchScore
    {
        public int PlayerScore;
        public int BotScore;
    }
}

﻿using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Unit;
using Assets.Scripts.Unit.GameScene;
using System;

namespace Assets.Scripts.Match
{
    internal class InitiativeCheckController : IController, IDisposable
    {
        private DIContainer container;
        private MatchController matchController;
        private RoundController roundController;
        private PlayerController playerController;
        private BotController botController;
        
        public InitiativeCheckController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            matchController = container.Get<MatchController>();
            roundController = container.Get<RoundController>();
            playerController = container.Get<PlayerController>();
            botController = container.Get<BotController>();

            roundController.OnNextRound += Check;
        }

        public void Dispose()
        {
            roundController.OnNextRound -= Check;
        }

        private void Check()
        {
            int playerIni = playerController.GetUnitStats().Initiative;
            int botIni = botController.GetUnitStats().Initiative;

            if (playerIni < 1 && botIni < 1)
            {
                matchController.NotEnoughInitiative(UnitManager.None);
                return;
            }

            if (playerIni < 1)
            {
                matchController.NotEnoughInitiative(UnitManager.Bot);
                return;
            }

            if (botIni < 1)
            {
                matchController.NotEnoughInitiative(UnitManager.Player);
            }
        }
    }
}

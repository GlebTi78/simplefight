﻿using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.FightModel;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using System;
using UnityEngine;

namespace Assets.Scripts.Match
{
    internal class RoundController : IController
    {
        public event Action OnNextRound;
        public event Action OnRoundEnded;
        public event Action OnFinalRound;

        private DIContainer container;
        private MatchController matchController;
        private NextRoundFeaturesHolder featuresHolder;
        private int maxRound;

        public RoundController(DIContainer container)
        {
            this.container = container;
        }

        public int RoundCount { get; private set; } = 1;
        public void Init()
        {
            matchController = container.Get<MatchController>();
            featuresHolder = container.Get<NextRoundFeaturesHolder>();
            maxRound = Resources.Load<MatchSettings>("MatchSettings").RoundsCount;
        }

        public void RoundCompleted()
        {
            OnRoundEnded.Invoke();
            NewRound();
        }

        private void NewRound()
        {
            RoundCount++;
            if (RoundCount > maxRound)
            {
                matchController.MatchCompleted();
                //OnNextRound.Invoke(); //temporary
                return;
            }

            if (RoundCount == maxRound)
            {
                Debug.Log($"[{this.GetType().Name}] Заключительный раунд");
                OnFinalRound.Invoke();
            }

            featuresHolder.ApplyFeatures(); //должен быть перед OnNextRound.Invoke()
            OnNextRound.Invoke();
        }
    }
}

﻿using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.UI.GameScene;
using Assets.Scripts.Unit;
using UnityEngine;

namespace Assets.Scripts.Match
{
    internal class ScoreController : IController
    {
        private DIContainer container;
        private UIController uiController;
        private int playerScore;
        private int botScore;

        public ScoreController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            uiController = container.Get<UIController>();
        }

        public void ScoreIncrease(UnitManager unitManager)
        {
            if (unitManager == UnitManager.Player)
                playerScore++;
            else if (unitManager == UnitManager.Bot)
                botScore++;
            else
                Debug.LogError($"[{this.GetType().Name}] ошибка определения UnitManager");

            uiController.UpdateScore(playerScore, botScore);
        }

        //public UnitManager GetWinner()
        //{
        //    if (playerScore == botScore)
        //        return UnitManager.None;
        //    else if (playerScore > botScore)
        //        return UnitManager.Player;
        //    else
        //        return UnitManager.Bot;
        //}

        public MatchScore GetScore()
        {
            MatchScore matchScore;
            matchScore.PlayerScore = playerScore;
            matchScore.BotScore = botScore;
            return matchScore;
        }
    }
}

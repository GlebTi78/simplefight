﻿using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.GameManagement
{
    internal class InWorkingSceneController : BaseSceneController, IController, ISceneController
    {
        protected override void Awake()
        {
            Disposables = new InWorkingSceneInit().Start(this, ConfigHolder);
        }
    }
}

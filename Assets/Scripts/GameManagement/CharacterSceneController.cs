﻿using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.GameManagement
{
    internal class CharacterSceneController : BaseSceneController, IController, ISceneController
    {
        protected override void Awake()
        {
            Disposables = new CharacterSceneInit().Start(this, ConfigHolder);
        }
    }
}

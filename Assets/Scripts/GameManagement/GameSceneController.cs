﻿using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.GameManagement
{
    internal class GameSceneController : BaseSceneController, IController, ISceneController
    {
        protected override void Awake()
        {
            Disposables = new GameSceneInit().Start(this, ConfigHolder);
        }

        public void StartNewMatch()
        {
            SceneManager.LoadScene((int)SceneType.GameScene);
        }
    }
}

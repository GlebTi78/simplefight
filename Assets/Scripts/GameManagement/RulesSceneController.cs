﻿using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.GameManagement
{
    internal class RulesSceneController : BaseSceneController, IController, ISceneController
    {
        protected override void Awake()
        {
            Disposables = new RulesSceneInit().Start(this, ConfigHolder);
        }
    }
}

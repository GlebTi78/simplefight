﻿namespace Assets.Scripts.GameManagement
{
    internal enum SceneType
    {
        MainMenuScene,
        GameScene,
        CharacterScene,
        MatchTypeSelectScene,
        DeckScene,
        RulesScene,
        InWorkingScene
    }
}

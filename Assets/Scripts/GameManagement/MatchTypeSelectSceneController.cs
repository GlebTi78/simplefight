﻿using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.GameManagement
{
    internal class MatchTypeSelectSceneController : BaseSceneController, IController, ISceneController
    {
        protected override void Awake()
        {
            Disposables = new MatchTypeSelectSceneInit().Start(this, ConfigHolder);
        }
    }
}

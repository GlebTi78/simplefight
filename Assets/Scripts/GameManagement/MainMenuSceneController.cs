﻿using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using UnityEngine;

namespace Assets.Scripts.GameManagement
{
    internal class MainMenuSceneController : BaseSceneController, IController, ISceneController
    {
        protected override void Awake()
        {
            Disposables = new MainMenuSceneInit().Start(this, ConfigHolder);
        }
    }
}

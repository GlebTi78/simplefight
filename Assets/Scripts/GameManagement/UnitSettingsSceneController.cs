﻿using Assets.Scripts.Configs;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Save;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameManagement
{
    internal class UnitSettingsSceneController : MonoBehaviour, IController, ISceneController
    {
        [SerializeField] private ConfigHolder configHolder;
        private List<IDisposable> disposables;

        private void Awake()
        {
            disposables = new UnitSettingsSceneInit().Start(this, configHolder);
        }

        private void OnDestroy()
        {
            foreach (var e in disposables)
                e.Dispose();
        }

        public void Init()
        {
            Debug.Log($"[{this.GetType().Name}].Init");
        }
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.GameManagement
{
    internal abstract class BaseSceneController : MonoBehaviour, IController, ISceneController
    {
        [SerializeField] protected ConfigHolder ConfigHolder;
        protected List<IDisposable> Disposables;

        protected abstract void Awake();

        private void OnDestroy()
        {
            foreach (var e in Disposables)
                e.Dispose();
        }

        public void Init()
        {
        }
    }
}

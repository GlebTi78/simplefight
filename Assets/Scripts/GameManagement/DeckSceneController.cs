﻿using Assets.Scripts.GameManagement;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.Configs.MatchTypeSelectScene
{
    internal class DeckSceneController : BaseSceneController, IController, ISceneController
    {
        protected override void Awake()
        {
            Disposables = new DeckSceneInit().Start(this, ConfigHolder);
        }
    }
}
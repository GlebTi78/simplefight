﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.DeckScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using System;
using Assets.Scripts.Cards;
using Assets.Scripts.GameManagement;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI.DeckScene
{
    internal class DeckSceneUIController : IController, IDisposable
    {
        private DIContainer container;
        private DeckSceneUIConfig config;
        private DeckSceneSelectedCardViewPanelConfig selectedCardConfig;

        public DeckSceneUIController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<DeckSceneUIConfig>();
            selectedCardConfig = container.Get<ConfigHolder>()
                .GetComponent<DeckSceneSelectedCardViewPanelConfig>();

            GetAllCards();

            foreach (var cardView in config.CardViews)
            {
                cardView.OnMouseClick += SelectCard;
            }

            config.AddCardButton.onClick.AddListener(AddCardButtonDown);
            config.ExitButton.onClick.AddListener(() => SceneManager.LoadScene((int)SceneType.MainMenuScene));
        }

        public void Dispose()
        {
            foreach (var cardView in config.CardViews)
            {
                cardView.OnMouseClick -= SelectCard;
            }
        }

        private void GetAllCards()
        {
            BaseCard[] allCards = new AllCards().Get();

            for (int i = 0; i < allCards.Length; i++)
            {
                var card = allCards[i];
                config.CardViews[i].SetCard(card);
            }
        }

        private void SelectCard(BaseCard card)
        {
            selectedCardConfig.CardViewPanel.SetActive(true);
            selectedCardConfig.CardNameText.text = card.NameRu;
            selectedCardConfig.PowerText.text = $"{card.Power}";
            selectedCardConfig.FeatureText.text = card.FeatureDescription;
            selectedCardConfig.Image.sprite = card.BigImage;
        }

        private void AddCardButtonDown()
        {
            selectedCardConfig.CardViewPanel.SetActive(false);
        }
    }
}

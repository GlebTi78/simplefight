﻿using Assets.Scripts.UI.GameScene;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.DeckScene
{
    internal class DeckSceneUIConfig : MonoBehaviour
    {
        [SerializeField] private CardView[] cardViews;

        [SerializeField] private Button deckButton;
        [SerializeField] private Button addCardButton;
        [SerializeField] private Button removeCardButton;
        [SerializeField] private Button helpButton;
        [SerializeField] private Button exitButton;

        public CardView[] CardViews => cardViews;
        public Button DeckButton => deckButton;
        public Button AddCardButton => addCardButton;
        public Button RemoveCardButton => removeCardButton;
        public Button HelpButton => helpButton;
        public Button ExitButton => exitButton;
    }
}

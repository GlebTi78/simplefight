﻿namespace Assets.Scripts.Interfaces
{
    internal interface ITimerClient
    {
        void TimerFinish();
    }
}

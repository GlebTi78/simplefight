﻿namespace Assets.Scripts.Interfaces
{
    internal interface IFirstUpdate
    {
        void FirstUpdate();
    }
}

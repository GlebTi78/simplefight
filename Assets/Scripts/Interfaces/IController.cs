﻿namespace Assets.Scripts.Interfaces
{
    internal interface IController
    {
        void Init();
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using Assets.Scripts.Unit.GameScene;
using System;

namespace Assets.Scripts.UI.GameScene
{
    internal class PlayerBoostController : IController, IDisposable
    {
        private DIContainer container;
        private BoostButtonsConfig boostButtonsConfig;
        //private PlayerStatsPanelConfig playerStatsPanelConfig;
        private PlayerStatsPanelController playerStatsPanelController;
        private RoundController roundController;
        private PlayerController playerController;
        private int initiativeBoost;

        public int InitiativeBoost => initiativeBoost;

        public PlayerBoostController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            boostButtonsConfig = container.Get<ConfigHolder>().GetComponent<BoostButtonsConfig>();
            //playerStatsPanelConfig = container.Get<ConfigHolder>().GetComponent<PlayerStatsPanelConfig>();
            playerStatsPanelController = container.Get<PlayerStatsPanelController>();
            roundController = container.Get<RoundController>();
            playerController = container.Get<PlayerController>();
            initiativeBoost = 0;

            boostButtonsConfig.PlusButton.onClick.AddListener(Plus);
            boostButtonsConfig.MinusButton.onClick.AddListener(Minus);
            roundController.OnNextRound += NewRound;
        }

        public void Dispose()
        {
            boostButtonsConfig.PlusButton.onClick.RemoveAllListeners();
            boostButtonsConfig.MinusButton.onClick.RemoveAllListeners();
            roundController.OnNextRound -= NewRound;
        }

        private void Plus()
        {
            int playerIni = playerController.GetUnitStats().Initiative;
            if (playerIni > 0)
            {
                initiativeBoost++;
                playerController.UpdateInitiative(-1);
                UpdateView();
            }
        }

        private void Minus()
        {
            int playerIni = playerController.GetUnitStats().Initiative;
            if (initiativeBoost > 0)
            {
                initiativeBoost--;
                playerController.UpdateInitiative(1);
                UpdateView();
            }
        }

        private void NewRound()
        {
            initiativeBoost = 0;
        }
        
        private void UpdateView()
        {
            playerStatsPanelController.UpdateIniBoostView(initiativeBoost);
        }
    }
}

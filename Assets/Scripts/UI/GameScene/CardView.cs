﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Assets.Scripts.Cards;
using Assets.Scripts.Cards.Features;
using TMPro;

namespace Assets.Scripts.UI.GameScene
{
    internal class CardView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public event Action<BaseCard> OnMouseEnter;
        public event Action OnMouseExit;
        public event Action<BaseCard> OnMouseClick;

        [SerializeField] private TMP_Text cardNameText;
        [SerializeField] private TMP_Text powerText;
        [SerializeField] private TMP_Text iniText;
        [SerializeField] private TMP_Text concentrationText;
        [SerializeField] private TMP_Text featureText;
        [SerializeField] private Image image;
        
        private BaseCard card;
        private Button button;

        private void Awake()
        {
            button = GetComponent<Button>();
            button?.onClick.AddListener(() => OnMouseClick.Invoke(card));
        }

        private void OnDestroy()
        {
            button?.onClick.RemoveAllListeners();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            OnMouseEnter?.Invoke(card);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            OnMouseExit?.Invoke();
        }

        public void SetCard(BaseCard card)
        {
            this.card = card;
            image.sprite = card.SmallImage;
            cardNameText.text = card.NameRu;
            powerText.text = $"{card.Power}";

            if (card.FeatureType == FeatureType.None)
                featureText.text = "Способностей нет";
            else
                featureText.text = $"{card.FeatureDescription}";
        }
        
        public BaseCard GetCard() => card;
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.GameScene
{
    internal class CharactersViewController : IController
    {
        private DIContainer container;
        private CharactersViewConfig config;
        
        public CharactersViewController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<CharactersViewConfig>();

            MatchController matchController = container.Get<MatchController>();
            byte characterId = (byte)matchController.Character.Id;
            byte botId = (byte)matchController.Bot.Id;

            SetCharacterImage(characterId, config.CharacterViewImage);
            SetCharacterImage(botId, config.BotViewImage);
        }

        private void SetCharacterImage(byte id, Image image)
        {
            switch (id)
            {
                case 1:
                    image.sprite = config.CharacterSouthSprite;
                    break;

                case 2:
                    image.sprite = config.CharacterNorthSprite;
                    break;

                case 3:
                    image.sprite = config.CharacterWestSprite;
                    break;

                case 4:
                    image.sprite = config.CharacterEastSprite;
                    break;

                default:
                    Debug.Log($"[{this.GetType().Name}] ошибка определения персонажа");
                    image.sprite = config.CharacterSouthSprite;
                    break;
            }
        }
    }
}

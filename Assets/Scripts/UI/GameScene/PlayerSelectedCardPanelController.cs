﻿using Assets.Scripts.Cards;
using Assets.Scripts.Configs;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Unit.GameScene;
using System;
using UnityEngine;

namespace Assets.Scripts.UI.GameScene
{
    internal class PlayerSelectedCardPanelController : UnitSelectedCardPanelController, 
        IController, IDisposable, IFirstUpdate
    {
        private PlayerController playerController;

        public PlayerSelectedCardPanelController(DIContainer container) : base(container)
        {
        }

        public override void Init()
        {
            base.Init();
            playerController = Container.Get<PlayerController>();
            Config = Container.Get<ConfigHolder>().GetComponent<PlayerSelectedCardViewConfig>();
            CardsOnPanels = GameObject.FindObjectsOfType<CardView>();

            foreach (CardView cardView in CardsOnPanels)
            {
                cardView.OnMouseClick += ActivatePanel;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            foreach (CardView cardView in CardsOnPanels)
            {
                cardView.OnMouseClick -= ActivatePanel;
            }
        }

        public override void ActivatePanel(BaseCard card)
        {
            Config.CardViewPanel.SetActive(true);
            SetCard(card);
            UiController.ButtonFightEnabled(true);
            playerController.SetSelectedCard(card);
        }
    }
}

﻿using Assets.Scripts.Cards;
using Assets.Scripts.Configs;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.GameScene
{
    internal class CentralCardPanelController : IDisposable, IController
    {
        private CardViewConfig config;
        private DIContainer container;
        private CardView[] cardsOnPanels;

        public CentralCardPanelController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<CentralCardViewConfig>();
            cardsOnPanels = GameObject.FindObjectsOfType<CardView>();
            foreach (CardView cardView in cardsOnPanels)
            {
                cardView.OnMouseEnter += ActivatePanel;
                cardView.OnMouseExit += DeactivatePanel;
            }
            DeactivatePanel();
        }

        public void Dispose()
        {
            foreach (CardView cardView in cardsOnPanels)
            {
                cardView.OnMouseEnter -= ActivatePanel;
                cardView.OnMouseExit -= DeactivatePanel;
            }
        }

        private void ActivatePanel(BaseCard card)
        {
            config.CardViewPanel.SetActive(true);
            SetCard(card);
        }

        private void DeactivatePanel()
        {
            config.CardViewPanel.SetActive(false);
        }

        private void SetCard(BaseCard card)
        {
            config.CardNameText.text = card.NameRu;
            config.PowerText.text = $"{card.Power}";
            config.FeatureText.text = card.FeatureDescription;
            config.CardViewPanel.gameObject.GetComponent<Image>().sprite = card.BigImage;
        }
    }
}

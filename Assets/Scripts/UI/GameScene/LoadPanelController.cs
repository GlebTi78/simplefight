﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using DG.Tweening;

namespace Assets.Scripts.UI.GameScene
{
    internal class LoadPanelController : IController
    {
        private DIContainer container;
        private LoadPanelConfig config;

        public LoadPanelController(DIContainer container)
        {
            this.container = container;
        }
        
        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<LoadPanelConfig>();
            LoadEffect();
        }

        private void LoadEffect()
        {
            float duration = 2.1f;
            float duration2 = 2f;

            var sequence = DOTween.Sequence();

            sequence.Append(config.Image.DOFade(0.99f, duration))
                .Append(config.Image.DOFade(0, duration2))
                .AppendCallback(DeactivatePanel)
                .SetLink(config.Image.gameObject, LinkBehaviour.KillOnDestroy);
        }

        private void DeactivatePanel()
        {
            config.LoadPanel.SetActive(false);
        }
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.GameManagement;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using Assets.Scripts.Save;
using Assets.Scripts.Timer;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI.GameScene
{
    internal class MatchResultPanelController : IController, IDisposable, ITimerClient
    {
        private DIContainer container;
        private MatchResultPanelConfig config;
        private GameSceneController gameController;
        private TimerController timerController;
        private bool isInfoPanelOpen;
        private string matchResultText;
        
        private bool isMatchSeriesCompleted;
        private byte winsNumber;
        private int roundsNumber;

        public MatchResultPanelController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<MatchResultPanelConfig>();
            gameController = container.Get<GameSceneController>();
            timerController = container.Get<ConfigHolder>().GetComponent<TimerController>();

            config.NewMatchButton.onClick.AddListener(NewMatch);
            config.StatisticsButton.onClick.AddListener(ActivateStatisticsPanel);
            config.MatchResultButton.onClick.AddListener(InfoPanelControl);
            config.ExitButton.onClick.AddListener(Exit);
        }

        public void Dispose()
        {
            config.NewMatchButton.onClick.RemoveAllListeners();
            config.StatisticsButton.onClick.RemoveAllListeners();
            config.MatchResultButton.onClick.RemoveAllListeners();
            config.ExitButton.onClick.RemoveAllListeners();
        }

        public void MatchFinish(MatchResultData data)
        {
            ProcessMatchResult(data);
        }

        public void MatchSeriesFinish(MatchResultData data)
        {
            isMatchSeriesCompleted = true;
            winsNumber = (byte)PlayerPrefs.GetInt(PlayerPrefsKeys.WinsNumber); //перенести в MatchSettingsHolder?
            roundsNumber = PlayerPrefs.GetInt(PlayerPrefsKeys.RoundsNumber); ;
            
            ProcessMatchResult(data);
        }

        public void TimerFinish()
        {
            config.MatchResultPanel.SetActive(true);

            if (isMatchSeriesCompleted)
                ActivateStatisticsData();
        }

        private void ProcessMatchResult(MatchResultData data)
        {
            matchResultText = data.ResultText;

            switch (data.MatchResult)
            {
                case MatchResultType.Win:
                    config.MatchResultImage.sprite = config.WinSprite;
                    break;

                case MatchResultType.Defeat:
                    config.MatchResultImage.sprite = config.DefeatSprite;
                    break;

                case MatchResultType.Draw:
                    config.MatchResultImage.sprite = config.DrawSprite;
                    break;

                default:
                    Debug.LogError($"[{this.GetType().Name}] ошибка определения результата матча");
                    break;
            }

            ActivatePanel();
        }
        
        private void ActivatePanel()
        {
            timerController.TimerStart(this, 0.3f);
        }
        
        private void InfoPanelControl()
        {
            if (isInfoPanelOpen)
            {
                config.MatchResultInfoPanel.SetActive(false);
                isInfoPanelOpen = false;
                return;
            }

            isInfoPanelOpen = true;
            config.MatchResultText.text = matchResultText;
            config.MatchResultInfoPanel.SetActive(true);
        }

        private void NewMatch()
        {
            gameController.StartNewMatch();
        }

        private void ActivateStatisticsData()
        {
            config.NewMatchButton.gameObject.SetActive(false);
            config.StatisticsButton.gameObject.SetActive(true);

            config.RoundText.text = $"Продержался раундов - {roundsNumber}";
            config.WinsText.text = $"Одержал побед - {winsNumber}";
        }

        private void ActivateStatisticsPanel()
        {
            if (config.StatisticsPanel.activeInHierarchy)
                config.StatisticsPanel.SetActive(false);
            else 
                config.StatisticsPanel.SetActive(true);
        }

        private void Exit()
        {
            SceneManager.LoadScene((int)SceneType.MainMenuScene);
        }
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.GameManagement;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using Assets.Scripts.Timer;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI.GameScene
{
    internal class UIController : IController, IDisposable//, ITimerClient
    {
        public event Action OnFightButtonDown;

        public UIConfig config;

        private MatchResultPanelController matchResultPanelController;
        private RoundController roundController;
        private DIContainer container;
        //private TimerController timerController;

        public UIController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<UIConfig>();
            matchResultPanelController = container.Get<MatchResultPanelController>();
            roundController = container.Get<RoundController>();
            //timerController = container.Get<ConfigHolder>().GetComponent<TimerController>();

            config.RestartButton.onClick.AddListener(NextButtonDown);
            config.ExitButton.onClick.AddListener(Exit);
            config.FightButton.onClick.AddListener(FightButtonDown);

            roundController.OnNextRound += NextRound;

            ButtonFightEnabled(false);
            config.RestartButton.enabled = false;
        }

        public void Dispose()
        {
            config.RestartButton.onClick.RemoveAllListeners();
            config.ExitButton.onClick.RemoveAllListeners();
            config.FightButton.onClick.RemoveAllListeners();

            roundController.OnNextRound -= NextRound;
        }

        public void ButtonFightEnabled(bool value)
        {
            config.FightButton.enabled = value;
        }

        public void UpdateScore(int playerScore, int botScore)
        {
            config.ScoreText.text = $"{playerScore} : {botScore}";
        }

        public void OpenResultGamePanel(string message)
        {
            config.ResultGamePanel.SetActive(true);
            config.ResultGameText.text = message;
        }
        
        private void FightButtonDown()
        {
            OnFightButtonDown.Invoke();
            ButtonFightEnabled(false);
            config.RestartButton.enabled = true;
            foreach (var button in config.PlayerCardsButtons)
            {
                button.enabled = false;
            }
        }

        private void NextButtonDown()
        {
            roundController.RoundCompleted();
            config.RestartButton.enabled = false;
        }

        private void Exit()
        {
            SceneManager.LoadScene((int)SceneType.MainMenuScene);
        }

        private void NextRound()
        {
            config.RoundText.text = $"{roundController.RoundCount}";
            
            ButtonFightEnabled(false);
            foreach (var button in config.PlayerCardsButtons)
            {
                button.enabled = true;
            }
        }

        public void MatchCompleted(MatchResultData data)
        {
            matchResultPanelController.MatchFinish(data);
            config.GamePanel.SetActive(false);
        }

        public void MatchSeriesCompleted(MatchResultData data)
        {
            matchResultPanelController.MatchSeriesFinish(data);
            config.GamePanel.SetActive(false);
        }
    }
}

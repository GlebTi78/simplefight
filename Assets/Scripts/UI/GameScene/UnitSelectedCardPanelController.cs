﻿using Assets.Scripts.Cards;
using Assets.Scripts.Configs;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using System;
using UnityEngine;

namespace Assets.Scripts.UI.GameScene
{
    internal abstract class UnitSelectedCardPanelController : IController, IDisposable, IFirstUpdate
    {
        protected CardViewConfig Config;
        protected DIContainer Container;
        protected CardView[] CardsOnPanels;
        protected UIController UiController;

        private RoundController roundController;

        public UnitSelectedCardPanelController(DIContainer container)
        {
            this.Container = container;
        }

        public virtual void Init()
        {
            UiController = Container.Get<UIController>();
            roundController = Container.Get<RoundController>();
            
            roundController.OnNextRound += DeactivatePanel;
        }

        public virtual void Dispose()
        {
            roundController.OnNextRound -= DeactivatePanel;
        }

        public void FirstUpdate()
        {
            DeactivatePanel();
        }

        public abstract void ActivatePanel(BaseCard card);

        protected void SetCard(BaseCard card)
        {
            Config.CardNameText.text = card.NameRu;
            Config.PowerText.text = $"{card.Power}";
            Config.FeatureText.text = card.FeatureDescription;
            Config.Image.sprite = card.BigImage;
        }

        private void DeactivatePanel()
        {
            Config.CardViewPanel.SetActive(false);
        }
    }
}

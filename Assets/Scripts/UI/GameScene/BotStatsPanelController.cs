﻿using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Configs;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.UI.GameScene
{
    internal class BotStatsPanelController : UnitStatsPanelController, IController
    {
        public BotStatsPanelController(DIContainer container) : base(container)
        {
        }

        public override void Init()
        {
            Config = Container.Get<ConfigHolder>().GetComponent<BotStatsPanelConfig>();
        }
    }
}

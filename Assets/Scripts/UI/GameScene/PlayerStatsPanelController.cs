﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.UI.GameScene
{
    internal class PlayerStatsPanelController : UnitStatsPanelController, IController
    {
        public PlayerStatsPanelController(DIContainer container) : base(container)
        {
        }

        public override void Init()
        {
            Config = Container.Get<ConfigHolder>().GetComponent<PlayerStatsPanelConfig>();
        }
    }
}

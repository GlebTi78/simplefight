﻿using Assets.Scripts.Cards;
using Assets.Scripts.Configs;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Unit.GameScene;
using System;

namespace Assets.Scripts.UI.GameScene
{
    internal class BotSelectedCardPanelController : UnitSelectedCardPanelController, IController, IDisposable, IFirstUpdate
    {
        private BotController botController;
        
        public BotSelectedCardPanelController(DIContainer container) : base(container)
        {
        }

        public override void Init()
        {
            base.Init();
            Config = Container.Get<ConfigHolder>().GetComponent<BotSelectedCardViewConfig>();
            botController = Container.Get<BotController>();
        }

        public override void ActivatePanel(BaseCard card)
        {
            Config.CardViewPanel.SetActive(true);
            SetCard(card);
        }
    }
}

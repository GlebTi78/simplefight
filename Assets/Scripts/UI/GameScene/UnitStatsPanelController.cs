﻿using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Unit;
using DG.Tweening;
using UnityEngine.UI;

namespace Assets.Scripts.UI.GameScene
{
    internal abstract class UnitStatsPanelController : IController
    {
        protected DIContainer Container;
        protected StatsPanelConfig Config;

        protected UnitStatsPanelController(DIContainer container)
        {
            Container = container;
        }

        public abstract void Init();

        public void UpdateBaseStatsView(BaseUnitStats stats)
        {
            Config.Power.text = stats.Power.ToString();
            Config.Initiative.text = stats.Initiative.ToString();
            Config.Knockout.text = stats.Knockout.ToString();
        }

        public void UpdateIniBoostView(int value)
        {
            Config.InitiativeBoost.text = value.ToString();
        }

        public void LightEffectStart()
        {
            //LightEffect(Config.PowerLightImage);
            LightEffect(Config.InitiativeLightImage);
            LightEffect(Config.KnockoutLightImage);
            //LightEffect(Config.ConcentrationLightImage);
        }
        
        private void LightEffect(Image image)
        {
            float duration = 1f;

            var sequence = DOTween.Sequence();

            sequence.Append(image.DOFade(1, duration))
                .Append(image.DOFade(0, duration))
                .SetLink(image.gameObject, LinkBehaviour.KillOnDestroy);
        }
    }
}

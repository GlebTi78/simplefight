﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.RulesScene;
using Assets.Scripts.GameManagement;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI.RulesScene
{
    internal class RulesSceneUIController : IController, IDisposable
    {
        private DIContainer container;
        private RulesSceneUIConfig config;
        private int currentArrayIndex;

        public RulesSceneUIController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<RulesSceneUIConfig>();

            config.BackButton.enabled = false;

            config.BackButton.onClick.AddListener(Back);
            config.NextButton.onClick.AddListener(Next);
            config.ExitButton.onClick.AddListener(Exit);
        }

        public void Dispose()
        {
            config.BackButton.onClick.RemoveAllListeners();
            config.NextButton.onClick.RemoveAllListeners();
            config.ExitButton.onClick.RemoveAllListeners();
        }
        
        private void Next()
        {
            if (currentArrayIndex >= config.PageSprites.Length - 1) //дополнительная проверка
            {
                Debug.LogError($"[{this.GetType().Name}] выход за пределы массива");
                return;
            }

            currentArrayIndex++;
            config.RulesPanelImage.sprite = config.PageSprites[currentArrayIndex];

            config.BackButton.enabled = true;

            if (currentArrayIndex >= config.PageSprites.Length - 1)
            {
                config.NextButton.enabled = false;
            }
        }

        private void Back()
        {
            if (currentArrayIndex <= 0) //дополнительная проверка
            {
                Debug.LogError($"[{this.GetType().Name}] выход за пределы массива");
                return;
            }

            currentArrayIndex--;
            config.RulesPanelImage.sprite = config.PageSprites[currentArrayIndex];

            config.NextButton.enabled = true;

            if (currentArrayIndex <= 0)
            {
                config.BackButton.enabled = false;
            }
        }

        private void Exit()
        {
            SceneManager.LoadScene((int)SceneType.MainMenuScene);
        }
    }
}

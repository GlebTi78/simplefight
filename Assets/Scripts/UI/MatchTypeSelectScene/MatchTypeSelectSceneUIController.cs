﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.MatchTypeSelectScene;
using Assets.Scripts.GameManagement;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.MatchSeries;
using Assets.Scripts.Save;
using Assets.Scripts.Timer;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI.MatchTypeSelectScene
{
    internal class MatchTypeSelectSceneUIController : IController, IDisposable, ITimerClient
    {
        private DIContainer container;
        private MatchTypeSelectSceneUIConfig config;
        private TimerController timerController;

        public MatchTypeSelectSceneUIController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<MatchTypeSelectSceneUIConfig>();
            timerController = container.Get<ConfigHolder>().GetComponent<TimerController>();

            config.StartSingleMathButton.onClick.AddListener(() => LoadGameScene(CompetitionType.Standard));
            config.StartTiilDropMathButton.onClick.AddListener(() => LoadGameScene(CompetitionType.TillDrop));
        }

        public void Dispose()
        {
            config.StartSingleMathButton.onClick.RemoveAllListeners();
            config.StartTiilDropMathButton.onClick.RemoveAllListeners();
        }

        public void TimerFinish() 
        {
            config.LoadMatchPanel.SetActive(true);
            SceneManager.LoadScene((int)SceneType.GameScene);
        }

        private void LoadGameScene(CompetitionType competitionType)
        {
            PlayerPrefs.SetInt(PlayerPrefsKeys.IsTestMatch, 0); //temporary
            PlayerPrefs.SetInt(PlayerPrefsKeys.CompetitionType, (int)competitionType);


            if (competitionType == CompetitionType.TillDrop)
            {
                PlayerPrefs.SetInt(PlayerPrefsKeys.IsStartSeries, 1);
                PlayerPrefs.SetInt(PlayerPrefsKeys.WinsNumber, 0);
                PlayerPrefs.SetInt(PlayerPrefsKeys.RoundsNumber, 0);
            }

            timerController.TimerStart(this, 0.3f);
        }
    }
}

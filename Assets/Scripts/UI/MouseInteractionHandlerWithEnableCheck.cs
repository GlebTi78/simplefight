﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    internal class MouseInteractionHandlerWithEnableCheck : MouseInteractionHandler, 
        IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler
    {
        [SerializeField] private Button button;
        public override void OnPointerEnter(PointerEventData eventData)
        {
            if (button.enabled == false)
                return;
            base.OnPointerEnter(eventData);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            if (button.enabled == false)
                return;
            base.OnPointerDown(eventData);
        }
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.InWorkingScene;
using Assets.Scripts.Configs.RulesScene;
using Assets.Scripts.GameManagement;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using System;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI.InWorkingScene
{
    internal class InWorkingSceneUIController : IController, IDisposable
    {
        private DIContainer container;
        private InWorkingSceneUIConfig config;

        public InWorkingSceneUIController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<InWorkingSceneUIConfig>();

            config.BackButton.onClick.AddListener(Exit);
        }

        public void Dispose()
        {
            config.BackButton.onClick.RemoveAllListeners();
        }

        private void Exit()
        {
            SceneManager.LoadScene((int)SceneType.MainMenuScene);
        }
    }
}

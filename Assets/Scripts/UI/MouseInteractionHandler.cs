﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.UI
{
    public class MouseInteractionHandler : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler
    {
        public event Action OnMouseEnter;
        public event Action OnMouseExit;
        public event Action OnButtonClicked;

        [SerializeField] private GameObject buttonFrame;

        private void Awake()
        {
            buttonFrame?.SetActive(false);
        }

        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            buttonFrame?.SetActive(true);
            OnMouseEnter?.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            buttonFrame.SetActive(false);
            OnMouseExit?.Invoke();
        }
        public virtual void OnPointerDown(PointerEventData eventData)
        {
            OnButtonClicked?.Invoke();
        }
    }
}

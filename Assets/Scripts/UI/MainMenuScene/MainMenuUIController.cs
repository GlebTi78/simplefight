﻿using Assets.Scripts.Configs.MainMenuScene;
using Assets.Scripts.Configs;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using System;
using UnityEngine;
using Assets.Scripts.GameManagement;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI.MainMenuScene
{
    internal class MainMenuUIController : IController, IDisposable
    {
        private DIContainer container;
        private MainMenuUIConfig config;
        public MainMenuUIController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<MainMenuUIConfig>();

            config.HistoryButton.onClick.AddListener(() => SceneManager.LoadScene((int)SceneType.InWorkingScene));
            config.DeckButton.onClick.AddListener(() => SceneManager.LoadScene((int)SceneType.InWorkingScene));
            config.MatchButton.onClick.AddListener(LoadCharacterScene);
            config.RulesButton.onClick.AddListener(() => SceneManager.LoadScene((int)SceneType.RulesScene));
            config.ExitButton.onClick.AddListener(Exit);
        }

        public void Dispose()
        {
            foreach (var button in config.AllButtons)
            {
                button.onClick.RemoveAllListeners();
            }
        }

        private void LoadCharacterScene()
        {
            SceneManager.LoadScene((int)SceneType.CharacterScene);
            PlayerPrefs.DeleteAll();
        }

        private void Exit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}

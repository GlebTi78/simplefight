﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.CharacterScene;
using Assets.Scripts.GameManagement;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Save;
using Assets.Scripts.Timer;
using Assets.Scripts.Unit.Characters;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI.CharacterScene
{
    internal class CharacterSceneUIController : IController, IDisposable, ITimerClient
    {
        private DIContainer container;
        private CharacterSceneUIConfig config;
        private TimerController timerController;
        private CharacterViewPanelController characterViewPanelController;
        private byte characterId;

        public CharacterSceneUIController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<CharacterSceneUIConfig>();
            timerController = container.Get<ConfigHolder>().GetComponent<TimerController>();
            characterViewPanelController = container.Get<CharacterViewPanelController>();

            config.MatchButton.onClick.AddListener(LoadNextScene);

            config.TestMatchButton.onClick.AddListener(LoadGameSceneTestMatch);
            config.ExitButton.onClick.AddListener(Exit);

            config.ButtonShowCharacter_1.onClick.AddListener(() => CharacterViewPanelOpen(new Character_1()));
            config.ButtonShowCharacter_2.onClick.AddListener(() => CharacterViewPanelOpen(new Character_2()));
            config.ButtonShowCharacter_3.onClick.AddListener(() => CharacterViewPanelOpen(new Character_3()));
            config.ButtonShowCharacter_4.onClick.AddListener(() => CharacterViewPanelOpen(new Character_4()));

            config.ButtonCharacter_1.onClick.AddListener(() => characterId = 1);
            config.ButtonCharacter_2.onClick.AddListener(() => characterId = 2);
            config.ButtonCharacter_3.onClick.AddListener(() => characterId = 3);
            config.ButtonCharacter_4.onClick.AddListener(() => characterId = 4);
        }

        public void Dispose()
        {
            config.MatchButton.onClick.RemoveAllListeners();
            config.ExitButton.onClick.RemoveAllListeners();

            config.ButtonShowCharacter_1.onClick.RemoveAllListeners();
            config.ButtonShowCharacter_2.onClick.RemoveAllListeners();
            config.ButtonShowCharacter_3.onClick.RemoveAllListeners();
            config.ButtonShowCharacter_4.onClick.RemoveAllListeners();
        }

        public void TimerFinish()
        {
            //config.LoadMatchPanel.SetActive(true);
            SceneManager.LoadScene((int)SceneType.MatchTypeSelectScene);
        }

        private void CharacterViewPanelOpen(BaseCharacter character)
        {
            characterViewPanelController.Start(character);
            config.AllCharactersPanel.SetActive(false);
        }

        private void LoadNextScene()
        {
            PlayerPrefs.SetInt(PlayerPrefsKeys.CharacterId, characterId);

            timerController.TimerStart(this, 0.3f);
        }

        private void LoadGameSceneTestMatch()   //temporary
        {
            PlayerPrefs.SetInt(PlayerPrefsKeys.CharacterId, characterId);
            PlayerPrefs.SetInt(PlayerPrefsKeys.IsTestMatch, 1); //temporary

            timerController.TimerStart(this, 0.2f);
        }

        private void Exit()
        {
            SceneManager.LoadScene((int)SceneType.MainMenuScene);
        }
    }
}

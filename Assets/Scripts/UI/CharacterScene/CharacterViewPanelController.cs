﻿using Assets.Scripts.Cards;
using Assets.Scripts.Configs;
using Assets.Scripts.Configs.CharacterScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Unit.Characters;
using System;
using UnityEngine;

namespace Assets.Scripts.UI.CharacterScene
{
    internal class CharacterViewPanelController : IController, IDisposable
    {
        private DIContainer container;
        private CharacterViewPanelConfig config;
        private SelectedCardViewPanelConfig selectedCardConfig;
        
        public CharacterViewPanelController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<CharacterViewPanelConfig>();
            selectedCardConfig = container.Get<ConfigHolder>().GetComponent<SelectedCardViewPanelConfig>();

            config.BackButton.onClick.AddListener(ClosePanel);
            
            foreach (var cardView in config.CardViews)
            {
                cardView.OnMouseClick += SelectCard; //!
            }
        }

        public void Dispose()
        {
            config.BackButton.onClick.RemoveAllListeners();
            foreach (var cardView in config.CardViews)
            {
                cardView.OnMouseClick -= SelectCard;
            }
        }

        public void Start(BaseCharacter character)
        {
            config.CharacterViewPanel.SetActive(true);
            config.CharacterImage.sprite = character.Sprite;
            config.DescriptionText.text = character.Description;

            for (int i = 0; i < character.Deck.Length; i++)
            {
                var card = character.Deck[i];
                config.CardViews[i].SetCard(card);
            }
        }

        private void ClosePanel()
        {
            selectedCardConfig.CardViewPanel.SetActive(false);
            config.CharacterViewPanel.SetActive(false);
            config.AllCharactersPanel.SetActive(true);
        }

        private void SelectCard(BaseCard card)
        {
            selectedCardConfig.CardViewPanel.SetActive(true);
            selectedCardConfig.CardNameText.text = card.NameRu;
            selectedCardConfig.PowerText.text = $"{card.Power}";
            selectedCardConfig.FeatureText.text = card.FeatureDescription;
            selectedCardConfig.Image.sprite = card.BigImage;
        }
    }
}

﻿using Assets.Scripts.Cards;
using Assets.Scripts.Configs;
using Assets.Scripts.Configs.DeckScene;
using Assets.Scripts.GameManagement;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI.DeckScene
{
    internal class DeckSceneUIMainController : IController
    {
        private DIContainer container;
        private DeckSceneUIMainConfig config;
        private SelectedCardViewPanelController selectedCardViewPanelController;

        public DeckSceneUIMainController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<DeckSceneUIMainConfig>();
            selectedCardViewPanelController = container.Get<SelectedCardViewPanelController>();

            config.ExitButton.onClick.AddListener(Exit);
        }

        public void Dispose()
        {
            config.ExitButton.onClick.RemoveAllListeners();
        }

        public void SwitchPanel(DeckSceneReplaceablePanelsEnum panel)
        {
            if (panel == DeckSceneReplaceablePanelsEnum.AllCardsPanel)
            {
                config.AllCardsPanel.SetActive(true);
                config.DeckPanel.SetActive(false);
            }
            else
            {
                config.DeckPanel.SetActive(true);
                config.AllCardsPanel.SetActive(false);
            }

            DeactivateSelectedCardViewPanel();
        }

        public void ShowCard(BaseCard card)
        {
            config.SelectedCardViewPanel.SetActive(true);
            selectedCardViewPanelController.SetCard(card);
        }

        public void DeactivateSelectedCardViewPanel()
        {
            config.SelectedCardViewPanel.SetActive(false);
        }

        private void Exit()
        {
            SceneManager.LoadScene((int)SceneType.MainMenuScene);
        }
    }
}

﻿using Assets.Scripts.Configs.DeckScene;
using Assets.Scripts.Configs;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using System;
using UnityEngine;
using Assets.Scripts.Cards;

namespace Assets.Scripts.UI.DeckScene
{
    internal class DeckPanelController : IController, IDisposable
    {
        private DIContainer container;
        private DeckPanelConfig config;
        private DeckSceneUIMainController deckSceneUIMainController;

        public DeckPanelController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<DeckPanelConfig>();
            deckSceneUIMainController = container.Get<DeckSceneUIMainController>();

            GetDeck();

            foreach (var cardView in config.CardViews)
            {
                cardView.OnMouseClick += ShowCard;
            }


            config.RemoveCardButton.onClick.AddListener(RemoveCard);
            config.AllCardsButton.onClick.AddListener(SwitchPanel);
        }

        public void Dispose()
        {
            foreach (var cardView in config.CardViews)
            {
                cardView.OnMouseClick -= ShowCard;
            }

            config.RemoveCardButton.onClick.RemoveAllListeners();
            config.AllCardsButton.onClick.RemoveAllListeners();
        }

        private void RemoveCard()
        {
            Debug.Log($"[{this.GetType().Name}] карта удалена из колоды");
            deckSceneUIMainController.DeactivateSelectedCardViewPanel();
        }

        private void SwitchPanel()
        {
            deckSceneUIMainController.SwitchPanel(DeckSceneReplaceablePanelsEnum.AllCardsPanel);
        }

        private void GetDeck()
        {
            BaseCard[] deck = new AllCards().GetDeck();

            for (int i = 0; i < deck.Length; i++)
            {
                var card = deck[i];
                config.CardViews[i].SetCard(card);
            }
        }

        private void ShowCard(BaseCard card)
        {
            deckSceneUIMainController.ShowCard(card);
        }
    }
}

﻿using Assets.Scripts.Cards;
using Assets.Scripts.Configs;
using Assets.Scripts.Configs.DeckScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using System;
using UnityEngine;

namespace Assets.Scripts.UI.DeckScene
{
    internal class AllCardsPanelController : IController, IDisposable
    {
        private DIContainer container;
        private AllCardsPanelConfig config;
        private DeckSceneUIMainController deckSceneUIMainController;

        public AllCardsPanelController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<AllCardsPanelConfig>();
            deckSceneUIMainController = container.Get<DeckSceneUIMainController>();

            GetAllCards();

            foreach (var cardView in config.CardViews)
            {
                cardView.OnMouseClick += ShowCard;
            }

            config.AddCardButton.onClick.AddListener(AddCard);
            config.DeckButton.onClick.AddListener(SwitchPanel);
        }
        
        public void Dispose()
        {
            foreach (var cardView in config.CardViews)
            {
                cardView.OnMouseClick -= ShowCard;
            }

            config.AddCardButton.onClick.RemoveAllListeners();
            config.DeckButton.onClick.RemoveAllListeners();
        }

        private void AddCard()
        {
            Debug.Log($"[{this.GetType().Name}] карта добавлена");
            deckSceneUIMainController.DeactivateSelectedCardViewPanel();
        }

        private void SwitchPanel()
        {
            deckSceneUIMainController.SwitchPanel(DeckSceneReplaceablePanelsEnum.DeckPanel);
        }

        private void GetAllCards()
        {
            BaseCard[] allCards = new AllCards().Get();

            for (int i = 0; i < allCards.Length; i++)
            {
                var card = allCards[i];
                config.CardViews[i].SetCard(card);
            }
        }

        private void ShowCard(BaseCard card)
        {
            deckSceneUIMainController.ShowCard(card);
        }
    }
}

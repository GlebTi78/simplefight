﻿using Assets.Scripts.Cards;
using Assets.Scripts.Configs;
using Assets.Scripts.Configs.DeckScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;

namespace Assets.Scripts.UI.DeckScene
{
    internal class SelectedCardViewPanelController : IController
    {
        private DIContainer container;
        private DeckSceneSelectedCardViewPanelConfig config;

        public SelectedCardViewPanelController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            config = container.Get<ConfigHolder>().GetComponent<DeckSceneSelectedCardViewPanelConfig>();
        }

        public void SetCard(BaseCard card)
        {
            //config.CardViewPanel.SetActive(true); => вызывается в DeckSceneUIMainController
            config.CardNameText.text = card.NameRu;
            config.PowerText.text = $"{card.Power}";
            config.CardDescription.text = card.CardDescription;
            config.FeatureText.text = card.FeatureDescription;
            config.Image.sprite = card.BigImage;
        }
    }
}

﻿namespace Assets.Scripts.UI.DeckScene
{
    internal enum DeckSceneReplaceablePanelsEnum
    {
        AllCardsPanel,
        DeckPanel
    }
}

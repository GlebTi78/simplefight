﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Test
{
    internal class FPSCounter : MonoBehaviour
    {
        [SerializeField] private Text fpsText;
        [SerializeField] private int frequency = 1;

        private float _timer = 0f;

        void Update()
        {
            var deltaTime = Time.unscaledDeltaTime;

            _timer += deltaTime;

            if (_timer >= 1f / frequency)
            {
                _timer = 0f;
                var fps = Mathf.FloorToInt(1.0f / deltaTime);

                fpsText.text = $"FPS: {fps}";
            }
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Test
{
    internal class AnimationSettingsPanelController : MonoBehaviour
    {
        public Text text;
        public Button plusButton;
        public Button minusButton;

        private float speedValue = 1.0f;

        private void Awake()
        {
            plusButton.onClick.AddListener(Plus);
            minusButton.onClick.AddListener(Minus);
        }

        public float GetSpeed()
        {
            return speedValue;
        }
        
        private void Plus()
        {
            speedValue += 0.1f;
            View();
        }

        private void Minus()
        {
            speedValue -= 0.1f;
            if (speedValue < 0.1f)
                speedValue = 0.1f;

            View();
        }

        private void View()
        {
            text.text = speedValue.ToString("0.0");
        }
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using System;
using UnityEngine.UI;

namespace Assets.Scripts.Test
{
    internal class LogPanelController : IController, IDisposable
    {
        private Text logText;
        private RoundController roundController;
        private string resultText;
        private DIContainer container;

        public LogPanelController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            logText = container.Get<ConfigHolder>().GetComponent<InfoPanelConfig>().LogText;
            roundController = container.Get<RoundController>();

            resultText = "Round 1.";
            logText.text = resultText;

            roundController.OnNextRound += NextRound;
        }
        public void Dispose()
        {
            roundController.OnNextRound -= NextRound;
        }

        public void Handler(string text)
        {
            resultText = $"{resultText}\n{text}";
            logText.text = resultText;
        }

        private void NextRound()
        {
            resultText = $"Round {roundController.RoundCount}.";
            logText.text = resultText;
        }
    }
}

﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Test
{
    internal class TestAnimation : MonoBehaviour
    {
        public Animator Animator3;
        public Animator Animator4;
        public AnimationSettingsPanelController AnimationPanel;
        
        //private int idleIndex = 0;
        //private int attackIndex = 1;
        //private int attackDamageIndex = 2;
        //private int blockIndex = 3;
        //private int counterAttackIndex = 4;
        //private int damageIndex = 5;
        //private int defeatIndex = 6;
        //private int winIndex = 7;
        private int index;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                index = 1;

            if (Input.GetKeyDown(KeyCode.Alpha2))
                index = 2;

            if (Input.GetKeyDown(KeyCode.Alpha3))
                index = 3;

            if (Input.GetKeyDown(KeyCode.Alpha4))
                index = 4;

            if (Input.GetKeyDown(KeyCode.Alpha5))
                index = 5;

            if (Input.GetKeyDown(KeyCode.Alpha6))
                index = 6;

            if (Input.GetKeyDown(KeyCode.Alpha7))
                index = 7;

            ChangeAnimation();
        }

        private void ChangeAnimation()
        {
            Animator3.speed = AnimationPanel.GetSpeed();
            Animator4.speed = AnimationPanel.GetSpeed();
            
            Animator3.SetInteger("Index", index);
            Animator4.SetInteger("Index", index);

            index = 0;
        }

        private IEnumerator SetIndex()
        {
            yield return new WaitForSeconds(0.1f);
            index = 0;
        }

        private void Info()
        {
            // Получаем информацию о текущем состоянии аниматора
            AnimatorStateInfo currentState = Animator3.GetCurrentAnimatorStateInfo(0);

            // Получаем ссылку на проигрываемый анимационный клип
            AnimationClip currentClip = Animator3.GetCurrentAnimatorClipInfo(0)[0].clip;

            

            // Выводим информацию о проигрываемом анимационном клипе
            Debug.Log("Проигрываемый анимационный клип: " + currentClip.name);
        }

        

    }
}

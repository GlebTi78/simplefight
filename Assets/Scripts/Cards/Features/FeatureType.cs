﻿namespace Assets.Scripts.Cards.Features
{
    internal enum FeatureType
    {
        None,
        StatsModifier,
        BlockingOneCard,
        BlockingTwoCards,
        Counterattack,
        CompositeFeature
    }
}

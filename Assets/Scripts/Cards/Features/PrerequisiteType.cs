﻿namespace Assets.Scripts.Cards.Features
{
    internal enum PrerequisiteType
    {
        None,
        AttackLaunched,     //атака состоялась
        DefenseLaunched,    //защита состоялась
        AttackSuccessful,   //атака успешна
        DefenseSuccessful   //защита успешна
    }
}

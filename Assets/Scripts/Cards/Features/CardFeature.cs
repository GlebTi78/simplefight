﻿using Assets.Scripts.Configs.CardConfigs;
using Assets.Scripts.Unit;

namespace Assets.Scripts.Cards.Features
{
    internal class CardFeature
    {
        public readonly FeatureType FeatureType;
        public readonly PrerequisiteType Prerequisite;
        //public readonly FeatureActionRound ActionRound;
        public readonly bool IsWorksOnOpponent;
        public readonly bool IsCounterattack;
        public readonly BaseUnitStats UnitStatsMod;

        public CardFeature(CardConfig config)
        {
            FeatureType = config.FeatureType;
            Prerequisite = config.Prerequisite;
            //ActionRound = config.ActionRound;
            IsWorksOnOpponent = config.IsWorksOnOpponent;
            IsCounterattack = config.IsCounterattack;

            if (FeatureType == FeatureType.StatsModifier)
            {
                UnitStatsMod.Power = config.PowerMod;
                UnitStatsMod.Initiative = config.IniMod;
                UnitStatsMod.Knockout = config.KnockoutMod;
            }
        }

        public BaseUnitStats GetStatsMod() => UnitStatsMod;
    }
}

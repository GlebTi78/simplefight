﻿using Assets.Scripts.Unit;
using Assets.Scripts.Unit.GameScene;
using UnityEngine;

namespace Assets.Scripts.Cards.Features
{
    internal class NextRoundFeature
    {
        public readonly UnitController Unit;
        public readonly FeatureType FeatureType;
        public readonly BaseUnitStats UnitStatsMod;
        public readonly string CardName; //для тестов

        public NextRoundFeature(UnitController unit, FeatureType featureType, BaseUnitStats unitStatsMod, string cardName)
        {
            Unit = unit;
            FeatureType = featureType;
            UnitStatsMod = unitStatsMod;
            CardName = cardName;
        }

        public NextRoundFeature(UnitController unit, FeatureType featureType, string cardName)
        {
            Unit = unit;
            FeatureType = featureType;
            CardName = cardName;
        }

        public void Apply()
        {
            switch (FeatureType)
            {
                case FeatureType.StatsModifier:
                    Unit.StatsMod(UnitStatsMod);
                    Debug.Log($"[{this.GetType().Name}] применена Feature карты {CardName}");
                    break;

                case FeatureType.BlockingOneCard:
                    Debug.Log($"[{this.GetType().Name}] применена Feature карты {CardName}");
                    Unit.CardBlocking(FeatureType);
                    break;

                case FeatureType.BlockingTwoCards:
                    Debug.Log($"[{this.GetType().Name}] применена Feature карты {CardName}");
                    Unit.CardBlocking(FeatureType);
                    break;

                default:
                    Debug.LogError($"[{this.GetType().Name}] ошибка определения FeatureType");
                    break;
            }
        }
    }
}

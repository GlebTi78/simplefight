﻿using Assets.Scripts.Cards.Features;
using Assets.Scripts.Configs.CardConfigs;
using UnityEngine;

namespace Assets.Scripts.Cards
{
    internal abstract class BaseCard
    {
        public readonly ActionType CardActionType;
        public readonly CardFeature Feature;
        public readonly PrivateCardStats PrivateStats;
        public readonly string CardDescription;  //художественное описание, к геймплею отношения не имеет

        private CardConfig config;
        private bool isHasBeenUsed;
        
        public BaseCard()
        {
            int id = InitId();
            if (id < 2000)                    //заменить
                CardActionType = ActionType.Attack;
            else
                CardActionType = ActionType.Defense;
            
            string fileName = $"ConfigCard_{id}";
            config = Resources.Load<CardConfig>(fileName);

            if (config.FeatureType != FeatureType.None)
            {
                Feature = new CardFeature(config);
            }

            PrivateStats.CardValue = config.CardValue;
            PrivateStats.InitiativeMinValue = config.InitiativeMinValue;
            PrivateStats.InitiativeMaxValue = config.InitiativeMaxValue;
            CardDescription = config.Description;
        }

        public string Name => config.Name;
        public string NameRu => config.NameRu;
        public int Power => config.Power;
        public FeatureType FeatureType => config.FeatureType;
        public PrerequisiteType Prerequisite => config.Prerequisite;
        public string FeatureDescription => config.FeatureDescription;
        public Sprite SmallImage => config.SmallImage;
        public Sprite BigImage => config.BigImage;
        public bool IsUsed => isHasBeenUsed;

        //public CardConfig GetConfig() => config;
        
        public void CardHasBeenUsed()
        {
            isHasBeenUsed = true;
        }

        private int InitId()
        {
            byte stringIndex = 5;
            string key = this.GetType().Name.Substring(stringIndex);
            int id = int.Parse(key);
            return id;
        }
    }
}

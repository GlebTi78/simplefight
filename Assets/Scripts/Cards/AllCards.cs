﻿using Assets.Scripts.Cards.AttackingCards;
using Assets.Scripts.Cards.DefenseCards;

namespace Assets.Scripts.Cards
{
    internal class AllCards
    {
        public BaseCard[] Get()
        {
            BaseCard[] allCards = new BaseCard[]
            {
                new Card_1001(),
                new Card_1002(),
                new Card_1003(),
                new Card_1004(),
                new Card_1005(),
                new Card_1006(),
                new Card_1007(),
                new Card_1008(),
                new Card_1009(),
                new Card_1010(),
                new Card_1011(),
                new Card_1012(),
                new Card_1013(),
                new Card_1014(),
                new Card_1015(),
                new Card_1016(),
                new Card_1017(),
                new Card_1018(),
                new Card_1019(),
                new Card_1020(),
                new Card_1021(),
                new Card_1022(),
                new Card_1023(),
                new Card_1024(),

                new Card_2001(),
                new Card_2002(),
                new Card_2003(),
                new Card_2004(),
                new Card_2005(),
                new Card_2006(),
                new Card_2007(),
                new Card_2008(),
                new Card_2009(),
                new Card_2010(),
                new Card_2011(),
                new Card_2012(),
                new Card_2013(),
                new Card_2014(),
                new Card_2015(),
                new Card_2016(),
                new Card_2017(),
                new Card_2018(),
                new Card_2019(),
                new Card_2020(),
                new Card_2021(),
                new Card_2022(),
                new Card_2023(),
                new Card_2024()
            };

            return allCards;
        }

        public BaseCard[] GetDeck() //temporary
        {
            BaseCard[] deck = new BaseCard[]
            {
                new Card_1001(),
                new Card_1002(),
                new Card_1003(),
                new Card_1004(),
                new Card_1005(),
                new Card_1006(),
                new Card_1007(),
                new Card_1008(),
                

                new Card_2001(),
                new Card_2002(),
                new Card_2003(),
                new Card_2004(),
                new Card_2005(),
                new Card_2006(),
                new Card_2007(),
                new Card_2008(),
            };

            return deck;
        }
    }
}

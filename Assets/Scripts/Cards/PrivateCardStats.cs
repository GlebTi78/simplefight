﻿namespace Assets.Scripts.Cards
{
    internal struct PrivateCardStats
    {
        public int CardValue;
        public int InitiativeMinValue;
        public int InitiativeMaxValue;
    }
}

﻿using Assets.Scripts.Cards.Features;
using Assets.Scripts.Cards;
using UnityEngine;

namespace Assets.Scripts.FightModel
{
    internal class PhaseOneConditionsCheckHandler
    {
        public bool IsAttackFeatureWorks(StateInRound stateInRound)
        {
            var attackCard = stateInRound.AttackingUnit.GetSelectedCard();
            var attackCardFeature = attackCard.Feature;

            if (attackCardFeature == null)
            {
                //Debug.Log($"[{this.GetType().Name}] У карты атаки нет способности.");
                return false;
            }

            if (attackCard.CardActionType != ActionType.Attack)
            {
                Debug.Log($"[{this.GetType().Name}] у атакующего юнита используется карта защиты");
                
                if (attackCardFeature.Prerequisite != PrerequisiteType.None)
                {
                    return false;
                }
            }

            if (attackCardFeature.Prerequisite != PrerequisiteType.AttackLaunched &&
                attackCardFeature.Prerequisite != PrerequisiteType.None)
            {
                //Debug.Log($"[{this.GetType().Name}] 2");
                return false;
            }

            if (attackCardFeature.Prerequisite == PrerequisiteType.AttackLaunched &&
                stateInRound.IsAttackWasLaunched == false)
            {
                //Debug.Log($"[{this.GetType().Name}] 3");
                return false;
            }



            //Debug.Log($"[{this.GetType().Name}] Атакующая карта: действует способность фазы 1");
            return true;
        }

        public bool IsDefenseFeatureWorks(StateInRound stateInRound)
        {
            var defenseCard = stateInRound.DefendingUnit.GetSelectedCard();
            var defenseCardFeature = defenseCard.Feature;

            if (defenseCardFeature == null)
            {
                //Debug.Log($"[{this.GetType().Name}] У карты защиты нет способности.");
                return false;
            }

            if (defenseCard.CardActionType != ActionType.Defense)
            {
                Debug.Log($"[{this.GetType().Name}] у защищаегося юнита используется карта атаки");
                
                if (defenseCardFeature.Prerequisite != PrerequisiteType.None)
                {
                    return false;
                }
            }

            if (defenseCardFeature.Prerequisite != PrerequisiteType.DefenseLaunched &&
                defenseCardFeature.Prerequisite != PrerequisiteType.None)
            {
                //Debug.Log($"[{this.GetType().Name}] 7");
                return false;
            }

            if (defenseCardFeature.Prerequisite == PrerequisiteType.DefenseLaunched &&
                stateInRound.IsDefenseWasLaunched == false)
            {
                Debug.Log($"[{this.GetType().Name}] 8");
                return false;
            }

            //Debug.Log($"[{this.GetType().Name}] Защитная карта: действует способность фазы 1");
            return true;
        }
    }
}

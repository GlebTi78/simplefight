﻿using Assets.Scripts.Cards.Features;
using UnityEngine;

namespace Assets.Scripts.FightModel
{
    internal class StatsModifierFeatureHandler : BaseFeatureHandler
    {
        public StatsModifierFeatureHandler(StateInRound stateInRound, NextRoundFeaturesHolder featuresHolder) : 
            base(stateInRound, featuresHolder)
        {
        }

        public void Attack()
        {
            attackCard = stateInRound.AttackingUnit.GetSelectedCard();
            attackCardFeature = attackCard.Feature;

            //Debug.Log($"[{this.GetType().Name}] Карта атаки: действует StatsModifier");
            if (attackCardFeature.IsWorksOnOpponent)
            {
                var unit = stateInRound.DefendingUnit;
                var featureType = FeatureType.StatsModifier;
                var statsMod = attackCardFeature.GetStatsMod();
                var cardName = attackCard.NameRu;

                var nextRoundFeature = new NextRoundFeature(unit, featureType, statsMod, cardName);
                featuresHolder.AddFeature(nextRoundFeature);
            }
            else
            {
                var unit = stateInRound.AttackingUnit;
                var featureType = FeatureType.StatsModifier;
                var statsMod = attackCardFeature.GetStatsMod();
                var cardName = attackCard.NameRu;

                var nextRoundFeature = new NextRoundFeature(unit, featureType, statsMod, cardName);
                featuresHolder.AddFeature(nextRoundFeature);
            }
        }

        public bool Defense()
        {
            defenseCard = stateInRound.DefendingUnit.GetSelectedCard();
            defenseCardFeature = defenseCard.Feature;

            //Debug.Log($"[{this.GetType().Name}] Карта защиты: действует StatsModifier");

            if (defenseCardFeature.IsWorksOnOpponent)
            {
                var unit = stateInRound.AttackingUnit;
                var featureType = FeatureType.StatsModifier;
                var statsMod = defenseCardFeature.GetStatsMod();
                var cardName = defenseCard.NameRu;

                var nextRoundFeature = new NextRoundFeature(unit, featureType, statsMod, cardName);
                featuresHolder.AddFeature(nextRoundFeature);
            }
            else
            {
                var unit = stateInRound.DefendingUnit;
                var featureType = FeatureType.StatsModifier;
                var statsMod = defenseCardFeature.GetStatsMod();
                var cardName = defenseCard.NameRu;

                var nextRoundFeature = new NextRoundFeature(unit, featureType, statsMod, cardName);
                featuresHolder.AddFeature(nextRoundFeature);
            }

            if (defenseCardFeature.IsCounterattack)
                return true;
            else
                return false;
        }
    }
}

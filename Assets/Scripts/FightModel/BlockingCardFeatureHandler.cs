﻿using Assets.Scripts.Cards.Features;
using UnityEngine;

namespace Assets.Scripts.FightModel
{
    internal class BlockingCardFeatureHandler : BaseFeatureHandler
    {
        public BlockingCardFeatureHandler(StateInRound stateInRound, NextRoundFeaturesHolder featuresHolder) : 
            base(stateInRound, featuresHolder)
        {
        }

        public void Attack(FeatureType featureType)
        {
            if (featureType != FeatureType.BlockingOneCard && featureType != FeatureType.BlockingTwoCards)
            {
                Debug.LogError($"[{this.GetType().Name}] ошибка данных метода атаки");
                return;
            }

            var targetUnit = stateInRound.DefendingUnit;
            var cardName = stateInRound.AttackingUnit.GetSelectedCard().NameRu;
            var nextRoundFeature = new NextRoundFeature(targetUnit, featureType, cardName);
            featuresHolder.AddFeature(nextRoundFeature);
        }

        public void Defense(FeatureType featureType)
        {
            if (featureType != FeatureType.BlockingOneCard && featureType != FeatureType.BlockingTwoCards)
            {
                Debug.LogError($"[{this.GetType().Name}] ошибка данных метода защиты");
                return;
            }

            //Debug.Log($"[{this.GetType().Name}] Defense()");
            var targetUnit = stateInRound.AttackingUnit;
            var cardName = stateInRound.DefendingUnit.GetSelectedCard().NameRu;
            var nextRoundFeature = new NextRoundFeature(targetUnit, featureType, cardName);
            featuresHolder.AddFeature(nextRoundFeature);
        }
    }
}

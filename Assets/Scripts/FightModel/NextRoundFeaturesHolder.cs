﻿using Assets.Scripts.Cards.Features;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.FightModel
{
    internal class NextRoundFeaturesHolder : IController
    {
        private DIContainer container;
        //private RoundController roundController;
        private List<NextRoundFeature> features;
        
        public NextRoundFeaturesHolder(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            //roundController = container.Get<RoundController>();
            features = new List<NextRoundFeature>();
        }

        public void AddFeature(NextRoundFeature feature)
        {
            features.Add(feature);
        }

        public void ApplyFeatures()
        {
            foreach (var feature in features)
            {
                feature.Apply();
            }

            features.Clear();
        }
    }
}

﻿using Assets.Scripts.Unit.GameScene;

namespace Assets.Scripts.FightModel
{
    internal class StateInRound
    {
        public UnitController AttackingUnit;
        public UnitController DefendingUnit;

        public bool IsParity;
        public bool IsAttackWasLaunched;
        public bool IsDefenseWasLaunched;
        public bool IsAttackSuccessful;
        public bool IsDefenseSuccessful;
    }
}

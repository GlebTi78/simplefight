﻿using Assets.Scripts.Cards;
using Assets.Scripts.Cards.Features;
using UnityEngine;

namespace Assets.Scripts.FightModel
{
    internal class PhaseTwoConditionsCheck
    {
        public bool IsAttackFeatureWorks(StateInRound stateInRound)
        {
            var attackCard = stateInRound.AttackingUnit.GetSelectedCard();
            var attackCardFeature = attackCard.Feature;

            if (attackCardFeature == null)
            {
                //Debug.Log($"[{this.GetType().Name}] У карты атаки нет способности.");
                return false;
            }

            if (attackCard.CardActionType != ActionType.Attack)
            {
                //Debug.Log($"[{this.GetType().Name}] 1");
                return false;
            }

            if (attackCardFeature.Prerequisite != PrerequisiteType.AttackSuccessful)
            {
                //Debug.Log($"[{this.GetType().Name}] 2");
                return false;
            }

            if (attackCardFeature.Prerequisite == PrerequisiteType.AttackSuccessful &&
                stateInRound.IsAttackSuccessful == false)
            {
                
                //Debug.Log($"[{this.GetType().Name}] 3");
                //Debug.Log($"[{this.GetType().Name}]{stateInRound.IsAttackSuccessful}");
                return false;
            }

            //Debug.Log($"[{this.GetType().Name}] Атакующая карта: действует способность фазы 2");
            return true;
        }

        public bool IsDefenseFeatureWorks(StateInRound stateInRound)
        {
            var defenseCard = stateInRound.DefendingUnit.GetSelectedCard();
            var defenseCardFeature = defenseCard.Feature;

            if (defenseCardFeature == null)
            {
                //Debug.Log($"[{this.GetType().Name}] У карты защиты нет способности.");
                return false;
            }

            if (defenseCard.CardActionType != ActionType.Defense)
            {
                //Debug.Log($"[{this.GetType().Name}] 5");
                return false;
            }

            if (defenseCardFeature.Prerequisite != PrerequisiteType.DefenseSuccessful)
            {
                //Debug.Log($"[{this.GetType().Name}] 7");
                return false;
            }

            if (defenseCardFeature.Prerequisite == PrerequisiteType.DefenseSuccessful &&
                stateInRound.IsDefenseSuccessful == false)
            {
                //Debug.Log($"[{this.GetType().Name}] 8");
                return false;
            }

            //Debug.Log($"[{this.GetType().Name}] Защитная карта: действует способность фазы 2");
            return true;
        }
    }
}

﻿using Assets.Scripts.Cards;
using Assets.Scripts.Test;
using Assets.Scripts.Unit.GameScene;
using UnityEngine;

namespace Assets.Scripts.FightModel
{
    internal class UnitsActionIndicator
    {
        public StateInRound Start(UnitController player, UnitController bot, LogPanelController logPanelController)
        {
            StateInRound stateInRound = new StateInRound();
            var playerCard = player.GetSelectedCard();
            var botCard = bot.GetSelectedCard();

            if (playerCard.CardActionType != botCard.CardActionType)
            {
                if (playerCard.CardActionType == ActionType.Attack)
                {
                    stateInRound.AttackingUnit = player;
                    stateInRound.DefendingUnit = bot;
                }
                else
                {
                    stateInRound.AttackingUnit = bot;
                    stateInRound.DefendingUnit = player;
                }

                Log(logPanelController, stateInRound);
                ActionLaunchedCheck(stateInRound, logPanelController);
                return stateInRound;
            }
            
            int playerIniBoost = player.GetInitiativeBoost();
            int botIniBoost = bot.GetInitiativeBoost();
            
            if (playerIniBoost == botIniBoost)
            {
                stateInRound.IsParity = true;
            }
            else if (playerIniBoost > botIniBoost)
            {
                if (playerCard.CardActionType == ActionType.Attack)
                {
                    stateInRound.AttackingUnit = player;
                    stateInRound.DefendingUnit = bot;
                }
                else
                {
                    stateInRound.AttackingUnit = bot;
                    stateInRound.DefendingUnit = player;
                }
            }
            else if (playerIniBoost < botIniBoost)
            {
                if (botCard.CardActionType == ActionType.Attack)
                {
                    stateInRound.AttackingUnit = bot;
                    stateInRound.DefendingUnit = player;
                }
                else
                {
                    stateInRound.AttackingUnit = player;
                    stateInRound.DefendingUnit = bot;
                }
            }
            else
            {
                Debug.LogError($"[{this.GetType().Name}] ошибка определения приоритета атаки");
            }

            Log(logPanelController, stateInRound);
            ActionLaunchedCheck(stateInRound, logPanelController);
            return stateInRound;
        }

        /// <summary>
        /// Проверка состояний типа "атака состоялась" и "защита состоялась".
        /// </summary>
        private void ActionLaunchedCheck(StateInRound stateInRound, LogPanelController log)
        {
            if (stateInRound.IsParity)
                return;
            
            if (stateInRound.AttackingUnit.GetSelectedCard().CardActionType == ActionType.Attack)
            {
                stateInRound.IsAttackWasLaunched = true;
                //log.Handler($"{stateInRound.AttackingUnit.GetUnitManager()}: атака состоялась.");
            }

            if (stateInRound.DefendingUnit.GetSelectedCard().CardActionType == ActionType.Defense)
            {
                stateInRound.IsDefenseWasLaunched = true;
                //log.Handler($"{stateInRound.DefendingUnit.GetUnitManager()}: защита состоялась.");
            }
        }

        private void Log(LogPanelController log, StateInRound actions)
        {
            if (actions.IsParity)
            {
                return;
            }
            else if (actions.AttackingUnit == null)
            {
                log.Handler("Ошибка определения приоритета атаки");
            }
            else
            {
                log.Handler($"{actions.AttackingUnit.GetUnitManager()} в атаке, " +
                    $"{actions.DefendingUnit.GetUnitManager()} в защите.");
            }
        }
    }
}

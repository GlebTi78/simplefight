﻿using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using Assets.Scripts.Test;
using Assets.Scripts.UI.GameScene;
using Assets.Scripts.Unit.GameScene;
using System;
using UnityEngine;

namespace Assets.Scripts.FightModel
{
    internal class FightController : IController, IDisposable
    {
        private DIContainer container;
        private FeatureController featureController;
        private UIController uiController;
        private MatchController matchController;
        private ScoreController scoreController;
        private LogPanelController logPanelController;
        private PlayerController player;
        private BotController bot;
        private UnitController attackingUnit;
        private UnitController defendingUnit;
        private StateInRound stateInRound;
        private bool isCounterattack;
        private int knockoutValue;

        public FightController(DIContainer container) 
        {
            this.container = container;
        }
        
        public void Init()
        {
            featureController = container.Get<FeatureController>();
            uiController = container.Get<UIController>();
            matchController = container.Get<MatchController>();
            scoreController = container.Get<ScoreController>();
            logPanelController = container.Get<LogPanelController>();
            player = container.Get<PlayerController>();
            bot = container.Get<BotController>();
            knockoutValue = Resources.Load<MatchSettings>("MatchSettings").KnockoutValue;

            uiController.OnFightButtonDown += Start;
        }

        public void Dispose()
        {
            uiController.OnFightButtonDown -= Start;
        }

        public void Counterattack()
        {
            isCounterattack = true;
        }

        private void Start()
        {
            player.GetStatsFromCard();
            player.ApplyInitiativeBoost();

            bot.SetSelectedCard();
            bot.GetStatsFromCard();
            bot.ApplyInitiativeBoost();
            
            bool isParity = CheckUnitsActions();
            
            if (isParity)
                return;
            
            featureController.Start(stateInRound);
            Calculation();
            featureController.PhaseTwoStart();
            if (isCounterattack)
                CounterattackStart();
        }

        /// <summary>
        /// Метод определяет, какой юнит атакует и какой защищается, или фиксирует паритет в текущем раунде.
        /// </summary>
        private bool CheckUnitsActions()
        {
            stateInRound = new UnitsActionIndicator().Start(player, bot, logPanelController);
            if (stateInRound.IsParity)
            {
                Log("Паритет.\nСпособности карт не будут применены.");
                return true;
            }

            attackingUnit = stateInRound.AttackingUnit;
            defendingUnit = stateInRound.DefendingUnit;

            return false;
        }

        private void Calculation()
        {
            if (attackingUnit.GetUnitStats().Power > defendingUnit.GetUnitStats().Power)
            {
                stateInRound.IsAttackSuccessful = true;
                Log($"{attackingUnit.GetUnitManager()} - атака успешна.");
                
                bool result = KnockoutEventCheck(attackingUnit);
                if (result)
                    return;

                attackingUnit.KnockoutValueIncrease();
                scoreController.ScoreIncrease(attackingUnit.GetUnitManager());
            }
            else
            {
                stateInRound.IsDefenseSuccessful = true;
                Log($"{defendingUnit.GetUnitManager()} - защита успешна.");
            }
        }

        private void CounterattackStart()
        {
            Log($"{stateInRound.DefendingUnit.GetUnitManager()} контратаковал.");
            if (defendingUnit.GetUnitStats().Power > attackingUnit.GetUnitStats().Power)
            {
                Log("Контратака успешна");
                
                bool result = KnockoutEventCheck(defendingUnit);
                if (result)
                    return;

                defendingUnit.KnockoutValueIncrease();
                scoreController.ScoreIncrease(defendingUnit.GetUnitManager());
            }
            else
            {
                Log("Контратака блокирована");
            }
            
            isCounterattack = false;
        }

        private bool KnockoutEventCheck(UnitController unit)
        {
            if (unit.GetUnitStats().Knockout >= knockoutValue)
            {
                matchController.KnockoutEvent(unit.GetUnitManager());
                return true;
            }

            return false;
        }

        private void Log(string text)
        {
            logPanelController.Handler($"--------------");
            logPanelController.Handler(text);
        }
    }
}

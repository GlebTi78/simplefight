﻿using Assets.Scripts.Cards;
using Assets.Scripts.Cards.Features;

namespace Assets.Scripts.FightModel
{
    internal abstract class BaseFeatureHandler
    {
        protected StateInRound stateInRound;
        protected NextRoundFeaturesHolder featuresHolder;
        protected BaseCard attackCard;
        protected BaseCard defenseCard;
        protected CardFeature attackCardFeature;
        protected CardFeature defenseCardFeature;

        public BaseFeatureHandler(StateInRound stateInRound, NextRoundFeaturesHolder featuresHolder)
        {
            this.stateInRound = stateInRound;
            this.featuresHolder = featuresHolder;
            attackCard = stateInRound.AttackingUnit.GetSelectedCard();
            defenseCard = stateInRound.DefendingUnit.GetSelectedCard();
            attackCardFeature = attackCard.Feature;
            defenseCardFeature = defenseCard.Feature;
        }
    }
}

﻿using Assets.Scripts.Cards;
using Assets.Scripts.Cards.Features;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using UnityEngine;

namespace Assets.Scripts.FightModel
{
    internal class FeatureController : IController
    {
        private DIContainer container;
        private NextRoundFeaturesHolder featuresHolder;
        private FightController fightController;
        private StateInRound stateInRound;
        private BaseCard attackCard;
        private BaseCard defenseCard;

        public FeatureController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            featuresHolder = container.Get<NextRoundFeaturesHolder>();
            fightController = container.Get<FightController>();
        }

        public void Start(StateInRound stateInRound)
        {
            this.stateInRound = stateInRound;
            attackCard = stateInRound.AttackingUnit.GetSelectedCard();
            defenseCard = stateInRound.DefendingUnit.GetSelectedCard();

            var check = new PhaseOneConditionsCheckHandler();
            bool result = check.IsAttackFeatureWorks(stateInRound);
            if (result)
                AttackOption();

            result = check.IsDefenseFeatureWorks(stateInRound);
            if (result)
                DefenseOption();
        }

        public void PhaseTwoStart()
        {
            var check = new PhaseTwoConditionsCheck();
            bool result = check.IsAttackFeatureWorks(stateInRound);

            if (result)
                AttackOption();

            result = check.IsDefenseFeatureWorks(stateInRound);
            if (result)
                DefenseOption();
        }
        
        private void AttackOption()
        {
            var attackCardFeature = attackCard.Feature;

            switch (attackCardFeature.FeatureType)
            {
                case FeatureType.StatsModifier:
                    new StatsModifierFeatureHandler(stateInRound, featuresHolder).Attack();
                    break;

                case FeatureType.BlockingOneCard:
                    new BlockingCardFeatureHandler(stateInRound, featuresHolder).Attack(FeatureType.BlockingOneCard);
                    //Debug.Log($"[{this.GetType().Name}] Карта атаки: действует BlockingOneCard");
                    break;

                case FeatureType.BlockingTwoCards:
                    new BlockingCardFeatureHandler(stateInRound, featuresHolder).Attack(FeatureType.BlockingTwoCards);
                    //Debug.Log($"[{this.GetType().Name}] Карта атаки: действует BlockingTwoCards");
                    break;

                case FeatureType.Counterattack:
                    Debug.Log($"[{this.GetType().Name}] Карта атаки: действует Counterattack\nКонтратака не будет выполнена");
                    break;

                case FeatureType.CompositeFeature:
                    Debug.Log($"[{this.GetType().Name}] Карта атаки: действует CompositeFeature");
                    break;

                default:
                    Debug.LogError($"[{this.GetType().Name}] ошибка метода PhaseOneAttackOption()");
                    break;
            }
        }

        private void DefenseOption()
        {
            var defenseCardFeature = defenseCard.Feature;

            switch (defenseCardFeature.FeatureType)
            {
                case FeatureType.StatsModifier:
                    bool isCounterattack = new StatsModifierFeatureHandler(stateInRound, featuresHolder).Defense();
                    if (isCounterattack)
                    {
                        fightController.Counterattack();
                        Debug.Log($"[{this.GetType().Name}] Карта защиты: действует контратака");
                    }
                    break;

                case FeatureType.BlockingOneCard:
                    //Debug.Log($"[{this.GetType().Name}] Карта защиты: действует BlockingOneCard");
                    new BlockingCardFeatureHandler(stateInRound, featuresHolder).Defense(FeatureType.BlockingOneCard);
                    break;

                case FeatureType.BlockingTwoCards:
                    //Debug.Log($"[{this.GetType().Name}] Карта защиты: действует BlockingTwoCards");
                    new BlockingCardFeatureHandler(stateInRound, featuresHolder).Defense(FeatureType.BlockingTwoCards);
                    break;

                case FeatureType.Counterattack:
                    fightController.Counterattack();
                    //Debug.Log($"[{this.GetType().Name}] Карта защиты: действует Counterattack");
                    break;

                case FeatureType.CompositeFeature:
                    Debug.Log($"[{this.GetType().Name}] Карта защиты: действует CompositeFeature");
                    break;

                default:
                    Debug.LogError($"[{this.GetType().Name}] ошибка метода PhaseOneDefenseOption()");
                    break;
            }
        }
    }
}

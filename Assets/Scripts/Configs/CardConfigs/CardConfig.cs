﻿using Assets.Scripts.Cards.Features;
using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs
{
    internal class CardConfig : ScriptableObject
    {
        [Header ("Public stats")]
        [SerializeField] private string cardName;
        [SerializeField] private string cardNameRu;
        [SerializeField] private string description; //художественное описание, к геймплею отношения не имеет
        [SerializeField] private int power;

        [Header("Private stats")]
        [SerializeField] private int cardValue;
        [SerializeField] private int initiativeMinValue;
        [SerializeField] private int initiativeMaxValue;

        [Header("Sprites")]
        [SerializeField] private Sprite smallImage;
        [SerializeField] private Sprite bigImage;

        [Header("Feature settings")]
        [SerializeField][Multiline(4)] private string feature;
        [SerializeField] private FeatureType featureType;
        [SerializeField] private PrerequisiteType prerequisite;
        [SerializeField] private bool isWorksOnOpponent;
        [SerializeField] private bool isCounterattack;

        [Header("Stats mod")]
        [SerializeField] private int powerMod;
        [SerializeField] private int iniMod;
        [SerializeField] private int knockoutMod;

        public string Name => cardName;
        public string NameRu => cardNameRu;
        public string Description => description;
        public int Power => power;
        public int CardValue => cardValue;
        public int InitiativeMinValue => initiativeMinValue;
        public int InitiativeMaxValue => initiativeMaxValue;
        public string FeatureDescription => feature;
        
        public Sprite SmallImage => smallImage;
        public Sprite BigImage => bigImage;
        
        public FeatureType FeatureType => featureType;
        public PrerequisiteType Prerequisite => prerequisite;
        public bool IsWorksOnOpponent => isWorksOnOpponent;
        public bool IsCounterattack => isCounterattack;

        public int PowerMod => powerMod;
        public int IniMod => iniMod;
        public int KnockoutMod => knockoutMod;
    }
}

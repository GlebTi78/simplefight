﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2021", menuName = "Data/Cards/Defense/Card_2021")]
    internal class ConfigCard_2021 : CardConfig
    {
    }
}

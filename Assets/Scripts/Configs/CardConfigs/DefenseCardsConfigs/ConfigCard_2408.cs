﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2408", menuName = "Data/Cards/Defense/Card_2408")]
    internal class ConfigCard_2408 : CardConfig
    {
    }
}

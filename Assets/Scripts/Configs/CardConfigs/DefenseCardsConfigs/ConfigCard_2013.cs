﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2013", menuName = "Data/Cards/Defense/Card_2013")]
    internal class ConfigCard_2013 : CardConfig
    {
    }
}

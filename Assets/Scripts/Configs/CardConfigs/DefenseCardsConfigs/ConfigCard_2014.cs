﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2014", menuName = "Data/Cards/Defense/Card_2014")]
    internal class ConfigCard_2014 : CardConfig
    {
    }
}

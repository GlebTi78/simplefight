﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2019", menuName = "Data/Cards/Defense/Card_2019")]
    internal class ConfigCard_2019 : CardConfig
    {
    }
}

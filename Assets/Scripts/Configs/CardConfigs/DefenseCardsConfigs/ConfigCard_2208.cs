﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2208", menuName = "Data/Cards/Defense/Card_2208")]
    internal class ConfigCard_2208 : CardConfig
    {
    }
}

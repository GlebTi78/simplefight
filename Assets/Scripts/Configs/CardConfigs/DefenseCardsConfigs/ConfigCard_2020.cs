﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2020", menuName = "Data/Cards/Defense/Card_2020")]
    internal class ConfigCard_2020 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2007", menuName = "Data/Cards/Defense/Card_2007")]
    internal class ConfigCard_2007 : CardConfig
    {
    }
}

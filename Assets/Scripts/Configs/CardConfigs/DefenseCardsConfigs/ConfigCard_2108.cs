﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2108", menuName = "Data/Cards/Defense/Card_2108")]
    internal class ConfigCard_2108 : CardConfig
    {
    }
}

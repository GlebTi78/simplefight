﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2011", menuName = "Data/Cards/Defense/Card_2011")]
    internal class ConfigCard_2011 : CardConfig
    {
    }
}

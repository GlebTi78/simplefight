﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2008", menuName = "Data/Cards/Defense/Card_2008")]
    internal class ConfigCard_2008 : CardConfig
    {
    }
}

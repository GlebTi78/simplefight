﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2023", menuName = "Data/Cards/Defense/Card_2023")]
    internal class ConfigCard_2023 : CardConfig
    {
    }
}

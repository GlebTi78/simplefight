﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2022", menuName = "Data/Cards/Defense/Card_2022")]
    internal class ConfigCard_2022 : CardConfig
    {
    }
}

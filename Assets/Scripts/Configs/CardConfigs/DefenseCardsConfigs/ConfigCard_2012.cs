﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2012", menuName = "Data/Cards/Defense/Card_2012")]
    internal class ConfigCard_2012 : CardConfig
    {
    }
}

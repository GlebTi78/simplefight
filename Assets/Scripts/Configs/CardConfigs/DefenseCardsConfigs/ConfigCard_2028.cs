﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2028", menuName = "Data/Cards/Defense/Card_2028")]
    internal class ConfigCard_2028 : CardConfig
    {
    }
}

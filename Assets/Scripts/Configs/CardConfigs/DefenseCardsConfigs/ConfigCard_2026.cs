﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2026", menuName = "Data/Cards/Defense/Card_2026")]
    internal class ConfigCard_2026 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2001", menuName = "Data/Cards/Defense/Card_2001")]
    internal class ConfigCard_2001 : CardConfig
    {
    }
}

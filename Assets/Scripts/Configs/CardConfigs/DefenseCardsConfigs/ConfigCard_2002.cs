﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2002", menuName = "Data/Cards/Defense/Card_2002")]
    internal class ConfigCard_2002 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2018", menuName = "Data/Cards/Defense/Card_2018")]
    internal class ConfigCard_2018 : CardConfig
    {
    }
}

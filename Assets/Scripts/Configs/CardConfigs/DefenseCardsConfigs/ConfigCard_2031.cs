﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2031", menuName = "Data/Cards/Defense/Card_2031")]
    internal class ConfigCard_2031 : CardConfig
    {
    }
}

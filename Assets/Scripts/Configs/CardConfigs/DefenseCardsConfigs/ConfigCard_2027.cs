﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2027", menuName = "Data/Cards/Defense/Card_2027")]
    internal class ConfigCard_2027 : CardConfig
    {
    }
}

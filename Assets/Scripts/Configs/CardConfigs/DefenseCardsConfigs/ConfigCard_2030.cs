﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2030", menuName = "Data/Cards/Defense/Card_2030")]
    internal class ConfigCard_2030 : CardConfig
    {
    }
}

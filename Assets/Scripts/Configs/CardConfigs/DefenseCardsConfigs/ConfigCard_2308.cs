﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2308", menuName = "Data/Cards/Defense/Card_2308")]
    internal class ConfigCard_2308 : CardConfig
    {
    }
}

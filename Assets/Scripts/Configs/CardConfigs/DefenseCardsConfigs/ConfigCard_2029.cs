﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2029", menuName = "Data/Cards/Defense/Card_2029")]
    internal class ConfigCard_2029 : CardConfig
    {
    }
}

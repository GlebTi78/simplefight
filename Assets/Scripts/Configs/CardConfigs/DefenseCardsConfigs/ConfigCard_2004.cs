﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2004", menuName = "Data/Cards/Defense/Card_2004")]
    internal class ConfigCard_2004 : CardConfig
    {
    }
}

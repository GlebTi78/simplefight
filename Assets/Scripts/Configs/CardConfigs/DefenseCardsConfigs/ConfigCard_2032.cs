﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2032", menuName = "Data/Cards/Defense/Card_2032")]
    internal class ConfigCard_2032 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2010", menuName = "Data/Cards/Defense/Card_2010")]
    internal class ConfigCard_2010 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2024", menuName = "Data/Cards/Defense/Card_2024")]
    internal class ConfigCard_2024 : CardConfig
    {
    }
}

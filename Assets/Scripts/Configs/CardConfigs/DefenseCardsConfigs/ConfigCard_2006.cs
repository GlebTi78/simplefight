﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2006", menuName = "Data/Cards/Defense/Card_2006")]
    internal class ConfigCard_2006 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2003", menuName = "Data/Cards/Defense/Card_2003")]
    internal class ConfigCard_2003 : CardConfig
    {
    }
}

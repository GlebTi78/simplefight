﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2017", menuName = "Data/Cards/Defense/Card_2017")]
    internal class ConfigCard_2017 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2009", menuName = "Data/Cards/Defense/Card_2009")]
    internal class ConfigCard_2009 : CardConfig
    {
    }
}

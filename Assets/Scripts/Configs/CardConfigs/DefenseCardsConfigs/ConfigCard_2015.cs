﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2015", menuName = "Data/Cards/Defense/Card_2015")]
    internal class ConfigCard_2015 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2016", menuName = "Data/Cards/Defense/Card_2016")]
    internal class ConfigCard_2016 : CardConfig
    {
    }
}

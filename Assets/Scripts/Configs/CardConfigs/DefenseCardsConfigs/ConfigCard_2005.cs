﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2005", menuName = "Data/Cards/Defense/Card_2005")]
    internal class ConfigCard_2005 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.DefenseCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_2025", menuName = "Data/Cards/Defense/Card_2025")]
    internal class ConfigCard_2025 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1020", menuName = "Data/Cards/Attack/Card_1020")]
    internal class ConfigCard_1020 : CardConfig
    {
    }
}

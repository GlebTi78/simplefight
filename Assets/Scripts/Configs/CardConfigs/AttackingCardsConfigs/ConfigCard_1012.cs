﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1012", menuName = "Data/Cards/Attack/Card_1012")]
    internal class ConfigCard_1012 : CardConfig
    {
    }
}

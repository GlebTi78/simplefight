﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1019", menuName = "Data/Cards/Attack/Card_1019")]
    internal class ConfigCard_1019 : CardConfig
    {
    }
}

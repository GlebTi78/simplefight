﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1015", menuName = "Data/Cards/Attack/Card_1015")]
    internal class ConfigCard_1015 : CardConfig
    {
    }
}

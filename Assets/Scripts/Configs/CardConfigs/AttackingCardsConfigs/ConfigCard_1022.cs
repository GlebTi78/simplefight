﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1022", menuName = "Data/Cards/Attack/Card_1022")]
    internal class ConfigCard_1022 : CardConfig
    {
    }
}

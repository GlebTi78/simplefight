﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1002", menuName = "Data/Cards/Attack/Card_1002")]
    internal class ConfigCard_1002 : CardConfig
    {
    }
}

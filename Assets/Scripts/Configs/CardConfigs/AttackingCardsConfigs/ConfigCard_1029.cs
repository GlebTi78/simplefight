﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1029", menuName = "Data/Cards/Attack/Card_1029")]
    internal class ConfigCard_1029 : CardConfig
    {
    }
}
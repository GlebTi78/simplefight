﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1026", menuName = "Data/Cards/Attack/Card_1026")]
    internal class ConfigCard_1026 : CardConfig
    {
    }
}
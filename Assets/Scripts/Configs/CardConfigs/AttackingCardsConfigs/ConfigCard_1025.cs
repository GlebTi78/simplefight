﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1025", menuName = "Data/Cards/Attack/Card_1025")]
    internal class ConfigCard_1025 : CardConfig
    {
    }
}
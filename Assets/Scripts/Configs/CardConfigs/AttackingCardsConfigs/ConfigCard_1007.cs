﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1007", menuName = "Data/Cards/Attack/Card_1007")]
    internal class ConfigCard_1007 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1009", menuName = "Data/Cards/Attack/Card_1009")]
    internal class ConfigCard_1009 : CardConfig
    {
    }
}

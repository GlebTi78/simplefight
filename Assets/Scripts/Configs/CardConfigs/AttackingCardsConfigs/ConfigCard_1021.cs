﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1021", menuName = "Data/Cards/Attack/Card_1021")]
    internal class ConfigCard_1021 : CardConfig
    {
    }
}

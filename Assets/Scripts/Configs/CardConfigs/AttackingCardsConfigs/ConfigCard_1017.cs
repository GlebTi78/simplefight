﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1017", menuName = "Data/Cards/Attack/Card_1017")]
    internal class ConfigCard_1017 : CardConfig
    {
    }
}

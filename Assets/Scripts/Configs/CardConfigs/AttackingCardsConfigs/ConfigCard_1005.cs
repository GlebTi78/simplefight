﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1005", menuName = "Data/Cards/Attack/Card_1005")]
    internal class ConfigCard_1005 : CardConfig
    {
    }
}

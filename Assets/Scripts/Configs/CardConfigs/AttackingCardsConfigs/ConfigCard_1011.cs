﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1011", menuName = "Data/Cards/Attack/Card_1011")]
    internal class ConfigCard_1011 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1031", menuName = "Data/Cards/Attack/Card_1031")]
    internal class ConfigCard_1031 : CardConfig
    {
    }
}
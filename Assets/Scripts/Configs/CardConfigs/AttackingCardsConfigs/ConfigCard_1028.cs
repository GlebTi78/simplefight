﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1028", menuName = "Data/Cards/Attack/Card_1028")]
    internal class ConfigCard_1028 : CardConfig
    {
    }
}
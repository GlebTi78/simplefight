﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1008", menuName = "Data/Cards/Attack/Card_1008")]
    internal class ConfigCard_1008 : CardConfig
    {
    }
}

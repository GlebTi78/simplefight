﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{

    [CreateAssetMenu(fileName = "ConfigCard_1013", menuName = "Data/Cards/Attack/Card_1013")]
    internal class ConfigCard_1013 : CardConfig
    {
    }
}

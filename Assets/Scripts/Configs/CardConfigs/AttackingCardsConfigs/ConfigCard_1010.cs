﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1010", menuName = "Data/Cards/Attack/Card_1010")]
    internal class ConfigCard_1010 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1030", menuName = "Data/Cards/Attack/Card_1030")]
    internal class ConfigCard_1030 : CardConfig
    {
    }
}
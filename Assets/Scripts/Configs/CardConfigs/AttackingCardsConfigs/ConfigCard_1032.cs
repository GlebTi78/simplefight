﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1032", menuName = "Data/Cards/Attack/Card_1032")]
    internal class ConfigCard_1032 : CardConfig
    {
    }
}
﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1016", menuName = "Data/Cards/Attack/Card_1016")]
    internal class ConfigCard_1016 : CardConfig
    {
    }
}

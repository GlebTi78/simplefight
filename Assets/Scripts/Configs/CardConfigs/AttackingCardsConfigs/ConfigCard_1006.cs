﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1006", menuName = "Data/Cards/Attack/Card_1006")]
    internal class ConfigCard_1006 : CardConfig
    {
    }
}

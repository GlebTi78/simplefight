﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1001", menuName = "Data/Cards/Attack/Card_1001")]
    internal class ConfigCard_1001 : CardConfig
    {
    }
}

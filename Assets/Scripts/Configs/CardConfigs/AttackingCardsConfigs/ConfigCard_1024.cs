﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1024", menuName = "Data/Cards/Attack/Card_1024")]
    internal class ConfigCard_1024 : CardConfig
    {
    }
}

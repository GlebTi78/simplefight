﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1027", menuName = "Data/Cards/Attack/Card_1027")]
    internal class ConfigCard_1027 : CardConfig
    {
    }
}
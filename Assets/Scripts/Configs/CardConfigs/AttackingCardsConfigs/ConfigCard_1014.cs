﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1014", menuName = "Data/Cards/Attack/Card_1014")]
    internal class ConfigCard_1014 : CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1003", menuName = "Data/Cards/Attack/Card_1003")]
    internal class ConfigCard_1003: CardConfig
    {
    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1004", menuName = "Data/Cards/Attack/Card_1004")]
    internal class ConfigCard_1004 : CardConfig
    {
    }
}

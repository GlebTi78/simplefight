﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1023", menuName = "Data/Cards/Attack/Card_1023")]
    internal class ConfigCard_1023 : CardConfig
    {
    }
}

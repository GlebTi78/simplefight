﻿using UnityEngine;

namespace Assets.Scripts.Configs.CardConfigs.AttackingCardsConfigs
{
    [CreateAssetMenu(fileName = "ConfigCard_1018", menuName = "Data/Cards/Attack/Card_1018")]
    internal class ConfigCard_1018 : CardConfig
    {
    }
}

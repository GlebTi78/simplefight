﻿using Assets.Scripts.UI.GameScene;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.DeckScene
{
    internal class DeckPanelConfig : MonoBehaviour
    {
        [SerializeField] private CardView[] cardViews;
        [SerializeField] private Button removeCardButton;
        [SerializeField] private Button allCardsButton;


        public CardView[] CardViews => cardViews;
        public Button RemoveCardButton => removeCardButton;
        public Button AllCardsButton => allCardsButton;
    }
}

﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.Configs.DeckScene
{
    internal class DeckSceneSelectedCardViewPanelConfig : CardViewConfig
    {
        [SerializeField] private TMP_Text cardDescription;

        public TMP_Text CardDescription => cardDescription;
    }
}

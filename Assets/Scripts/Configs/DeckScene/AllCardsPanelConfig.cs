﻿using Assets.Scripts.UI.GameScene;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.DeckScene
{
    internal class AllCardsPanelConfig : MonoBehaviour
    {
        [SerializeField] private CardView[] cardViews;
        [SerializeField] private TMP_Text messageText;
        [SerializeField] private Button addCardButton;
        [SerializeField] private Button deckButton;

        public CardView[] CardViews => cardViews;
        public TMP_Text MessageText => messageText;
        public Button AddCardButton => addCardButton;
        public Button DeckButton => deckButton;
    }
}

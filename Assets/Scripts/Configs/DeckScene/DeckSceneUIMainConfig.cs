﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.DeckScene
{
    internal class DeckSceneUIMainConfig : MonoBehaviour
    {
        [SerializeField] private GameObject allCardsPanel;
        [SerializeField] private GameObject deckPanel;
        [SerializeField] private GameObject selectedCardViewPanel;
        [SerializeField] private Button exitButton;

        public GameObject AllCardsPanel => allCardsPanel;
        public GameObject DeckPanel => deckPanel;
        public GameObject SelectedCardViewPanel => selectedCardViewPanel;
        public Button ExitButton => exitButton;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs
{
    internal class AllButtonsConfig : MonoBehaviour
    {
        [SerializeField] private Button[] allButtons;

        public Button[] AllButtons => allButtons;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.CharacterScene
{
    internal class CharacterSceneUIConfig : MonoBehaviour
    {
        [SerializeField] private GameObject allCharactersPanel;
        //[SerializeField] private GameObject loadMatchPanel;

        [SerializeField] private Button buttonCharacter_1;
        [SerializeField] private Button buttonCharacter_2;
        [SerializeField] private Button buttonCharacter_3;
        [SerializeField] private Button buttonCharacter_4;
        
        [SerializeField] private Button buttonShowCharacter_1;
        [SerializeField] private Button buttonShowCharacter_2;
        [SerializeField] private Button buttonShowCharacter_3;
        [SerializeField] private Button buttonShowCharacter_4;
        
        [SerializeField] private Button matchButton;
        [SerializeField] private Button testMatchButton;
        [SerializeField] private Button exitButton;

        public GameObject AllCharactersPanel => allCharactersPanel;
        //public GameObject LoadMatchPanel => loadMatchPanel;

        public Button ButtonCharacter_1 => buttonCharacter_1;
        public Button ButtonCharacter_2 => buttonCharacter_2;
        public Button ButtonCharacter_3 => buttonCharacter_3;
        public Button ButtonCharacter_4 => buttonCharacter_4;

        public Button ButtonShowCharacter_1 => buttonShowCharacter_1;
        public Button ButtonShowCharacter_2 => buttonShowCharacter_2;
        public Button ButtonShowCharacter_3 => buttonShowCharacter_3;
        public Button ButtonShowCharacter_4 => buttonShowCharacter_4;

        public Button MatchButton => matchButton;
        public Button TestMatchButton => testMatchButton;
        public Button ExitButton => exitButton;
    }
}

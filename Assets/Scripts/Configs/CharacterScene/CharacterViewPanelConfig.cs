﻿using Assets.Scripts.UI.GameScene;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.CharacterScene
{
    internal class CharacterViewPanelConfig : MonoBehaviour
    {
        [SerializeField] private GameObject characterViewPanel;
        [SerializeField] private GameObject allCharactersPanel;
        [SerializeField] private Image characterImage;
        [SerializeField] private CardView[] cardViews;
        [SerializeField] private TMP_Text descriptionText;
        [SerializeField] private Button backButton;


        public GameObject CharacterViewPanel => characterViewPanel;
        public GameObject AllCharactersPanel => allCharactersPanel;
        public Image CharacterImage => characterImage;
        public CardView[] CardViews => cardViews;
        public TMP_Text DescriptionText => descriptionText;
        public Button BackButton => backButton;
    }
}

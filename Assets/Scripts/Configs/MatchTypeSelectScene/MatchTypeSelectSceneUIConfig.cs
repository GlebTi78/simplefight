﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.MatchTypeSelectScene
{
    internal class MatchTypeSelectSceneUIConfig : MonoBehaviour
    {
        [SerializeField] private Button startSingleMathButton;
        [SerializeField] private Button startTiilDropMathButton;
        [SerializeField] private GameObject loadMatchPanel;

        public Button StartSingleMathButton => startSingleMathButton;
        public Button StartTiilDropMathButton => startTiilDropMathButton;
        public GameObject LoadMatchPanel => loadMatchPanel;
    }
}

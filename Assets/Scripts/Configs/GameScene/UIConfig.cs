﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.GameScene
{
    internal class UIConfig : MonoBehaviour
    {
        [SerializeField] private GameObject gamePanel;
        //[SerializeField] private GameObject playerCardsInfoPanel;
        [SerializeField] private GameObject resultGamePanel;
        
        [SerializeField] private Button restartButton;
        [SerializeField] private Button exitButton;
        [SerializeField] private Button fightButton;
        
        [SerializeField] private TMP_Text roundText;
        [SerializeField] private TMP_Text scoreText;
        [SerializeField] private TMP_Text resultGameText;
        
        [SerializeField] private Button[] playerCardsButtons;

        public GameObject GamePanel => gamePanel;
        //public GameObject PlayerCardsInfoPanel => playerCardsInfoPanel;
        public GameObject ResultGamePanel => resultGamePanel;

        public Button RestartButton => restartButton;
        public Button ExitButton => exitButton;
        public Button FightButton => fightButton;

        public TMP_Text RoundText => roundText;
        public TMP_Text ScoreText => scoreText;
        public TMP_Text ResultGameText => resultGameText;

        public Button[] PlayerCardsButtons => playerCardsButtons;
    }
}

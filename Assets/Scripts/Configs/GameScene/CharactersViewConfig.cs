﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.GameScene
{
    internal class CharactersViewConfig : MonoBehaviour
    {
        //[SerializeField] private GameObject charactersView;
        //[SerializeField] private TMP_Text characterName;
        //[SerializeField] private TMP_Text botName;

        //public GameObject CharactersView => charactersView;
        //public TMP_Text CharacterName => characterName;
        //public TMP_Text BotName => botName;

        [SerializeField] private Image characterViewImage;
        [SerializeField] private Image botViewImage;
        
        [SerializeField] private Sprite characterEastSprite;
        [SerializeField] private Sprite characterNorthSprite;
        [SerializeField] private Sprite characterSouthSprite;
        [SerializeField] private Sprite characterWestSprite;

        public Image CharacterViewImage => characterViewImage;
        public Image BotViewImage => botViewImage;

        public Sprite CharacterEastSprite => characterEastSprite;
        public Sprite CharacterNorthSprite => characterNorthSprite;
        public Sprite CharacterSouthSprite => characterSouthSprite;
        public Sprite CharacterWestSprite => characterWestSprite;
    }
}

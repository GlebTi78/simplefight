﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.GameScene
{
    internal class BoostButtonsConfig : MonoBehaviour
    {
        [SerializeField] private Button plusButton;
        [SerializeField] private Button minusButton;
        //[SerializeField] private TMP_Text initiativeValue;

        public Button PlusButton => plusButton;
        public Button MinusButton => minusButton;
        //public TMP_Text InitiativeValue => initiativeValue;
    }
}

﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.GameScene
{
    internal class MatchResultPanelConfig : MonoBehaviour
    {
        [SerializeField] private GameObject matchResultPanel;
        [SerializeField] private Image matchResultImage;
        [SerializeField] private Button newMatchButton;
        [SerializeField] private Button exitButton;
        
        [SerializeField] private Button matchResultButton;
        [SerializeField] private GameObject matchResultInfoPanel;
        [SerializeField] private TMP_Text matchResultText;
        
        [SerializeField] private Sprite winSprite;
        [SerializeField] private Sprite defeatSprite;
        [SerializeField] private Sprite drawSprite;

        //statistics
        [SerializeField] private GameObject statisticsPanel;
        [SerializeField] private Button statisticsButton;
        [SerializeField] private TMP_Text roundsText;
        [SerializeField] private TMP_Text winsText;


        public GameObject MatchResultPanel => matchResultPanel;
        public Image MatchResultImage => matchResultImage;
        public Button NewMatchButton => newMatchButton;
        public Button ExitButton => exitButton;
        
        public Button MatchResultButton => matchResultButton;
        public GameObject MatchResultInfoPanel => matchResultInfoPanel; 
        public TMP_Text MatchResultText => matchResultText; 
        
        public Sprite WinSprite => winSprite;
        public Sprite DefeatSprite => defeatSprite;
        public Sprite DrawSprite => drawSprite;

        //statistics
        public GameObject StatisticsPanel => statisticsPanel;
        public Button StatisticsButton => statisticsButton;
        public TMP_Text RoundText => roundsText;
        public TMP_Text WinsText => winsText;
    }
}

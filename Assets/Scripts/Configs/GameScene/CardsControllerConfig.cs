﻿using Assets.Scripts.UI.GameScene;
using UnityEngine;

namespace Assets.Scripts.Configs.GameScene
{
    internal abstract class CardsControllerConfig : MonoBehaviour
    {
        [SerializeField] private CardView cardA;
        [SerializeField] private CardView cardB;
        [SerializeField] private CardView cardC;
        [SerializeField] private GameObject blockingPanelCardB;
        [SerializeField] private GameObject blockingPanelCardC;

        public CardView CardA => cardA;
        public CardView CardB => cardB;
        public CardView CardC => cardC;
        public GameObject BlockingPanelCardC => blockingPanelCardB;
        public GameObject BlockingPanelCardD => blockingPanelCardC;
    }
}

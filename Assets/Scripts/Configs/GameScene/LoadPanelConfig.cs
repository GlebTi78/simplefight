﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.GameScene
{
    internal class LoadPanelConfig : MonoBehaviour
    {
        [SerializeField] private GameObject loadPanel;
        [SerializeField] private Image image;

        public GameObject LoadPanel => loadPanel;
        public Image Image => image;
    }
}

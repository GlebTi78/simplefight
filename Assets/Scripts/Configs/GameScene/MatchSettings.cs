﻿using UnityEngine;

namespace Assets.Scripts.Configs.GameScene
{
    [CreateAssetMenu(fileName = "MatchSettings", menuName = "Data/MatchSettings")]
    internal class MatchSettings : ScriptableObject
    {
        [SerializeField] private int roundsCount;
        [SerializeField] private int playerInitiativeStartValue;
        [SerializeField] private int botInitiativeStartValue;
        [SerializeField] private int knockoutValue;

        [Header("TillDrop")] 
        [SerializeField] private byte initiativeIncreased;
        

        public int RoundsCount => roundsCount;
        public int PlayerInitiativeStartValue => playerInitiativeStartValue;
        public int BotInitiativeStartValue => botInitiativeStartValue;
        public int KnockoutValue => knockoutValue;

        public byte InitiativeIncreased => initiativeIncreased;
    }
}

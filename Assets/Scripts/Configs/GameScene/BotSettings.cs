﻿using UnityEngine;

namespace Assets.Scripts.Configs.GameScene
{
    [CreateAssetMenu(fileName = "BotSettings", menuName = "Data/BotSettings")]
    internal class BotSettings : ScriptableObject
    {
        [SerializeField] private int difficultyIndex;
        [SerializeField] private int randomCardSelectionIndex;

        public int DifficultyIndex => difficultyIndex;
        public int RandomCardSelectionIndex => randomCardSelectionIndex;
    }
}

﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.GameScene
{
    internal abstract class StatsPanelConfig : MonoBehaviour
    {
        [SerializeField] private TMP_Text power;
        [SerializeField] private TMP_Text initiative;
        [SerializeField] private TMP_Text knockout;
        [SerializeField] private TMP_Text initiativeBoost;
        //[SerializeField] private TMP_Text concentration;

        [SerializeField] private Image powerLightImage;
        [SerializeField] private Image initiativeLightImage;
        [SerializeField] private Image knockoutLightImage;
        //[SerializeField] private Image concentrationLightImage;

        public TMP_Text Power => power;
        public TMP_Text Initiative => initiative;
        public TMP_Text Knockout => knockout;
        public TMP_Text InitiativeBoost => initiativeBoost;
        //public TMP_Text Concentration => concentration;

        public Image PowerLightImage => powerLightImage;
        public Image InitiativeLightImage => initiativeLightImage;
        public Image KnockoutLightImage => knockoutLightImage;
        //public Image ConcentrationLightImage => concentrationLightImage;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.MainMenuScene
{
    internal class MainMenuUIConfig : MonoBehaviour
    {
        [SerializeField] private Button[] allButtons;
        [SerializeField] private Button historyButton;
        [SerializeField] private Button deckButton;
        [SerializeField] private Button matchButton;
        [SerializeField] private Button rulesButton;
        [SerializeField] private Button exitButton;
        [SerializeField] private GameObject loadMatchPanel;

        public Button[] AllButtons => allButtons;
        public Button HistoryButton => historyButton;
        public Button DeckButton => deckButton;
        public Button MatchButton => matchButton;
        public Button RulesButton => rulesButton;
        public Button ExitButton => exitButton;
        public GameObject LoadMatchPanel => loadMatchPanel;

    }
}

﻿using UnityEngine;

namespace Assets.Scripts.Configs
{
    internal class ButtonSoundControllerConfig : MonoBehaviour
    {
        [SerializeField] private AudioSource audioSource;
        [SerializeField] private AudioClip mouseEnterSound;
        [SerializeField] private AudioClip mouseClickSound;

        public AudioSource AudioSource => audioSource;
        public AudioClip MouseEnterSound => mouseEnterSound;
        public AudioClip MouseClickSound => mouseClickSound;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.InWorkingScene
{
    internal class InWorkingSceneUIConfig : MonoBehaviour
    {
        [SerializeField] private Button backButton;

        public Button BackButton => backButton;
    }
}

﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs
{
    internal abstract class CardViewConfig : MonoBehaviour
    {
        [SerializeField] private GameObject cardViewPanel;
        [SerializeField] private Image image;
        [SerializeField] private TMP_Text cardNameText;
        [SerializeField] private TMP_Text powerText;
        [SerializeField] private TMP_Text iniText;
        [SerializeField] private TMP_Text concentrationText;
        [SerializeField] private TMP_Text featureText;

        public GameObject CardViewPanel => cardViewPanel;
        public Image Image => image;
        public TMP_Text CardNameText => cardNameText;
        public TMP_Text PowerText => powerText;
        public TMP_Text IniText => iniText;
        public TMP_Text ConcentrationText => concentrationText;
        public TMP_Text FeatureText => featureText;
    }
}

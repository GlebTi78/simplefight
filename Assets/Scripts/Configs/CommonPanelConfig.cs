﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs
{
    internal class CommonPanelConfig : MonoBehaviour
    {
        [SerializeField] private Button restartButton;
        [SerializeField] private Button exitButton;
        [SerializeField] private Button fightButton;
        [SerializeField] private TMP_Text roundText;
        [SerializeField] private GameObject resultGamePanel;
        [SerializeField] private TMP_Text resultGameText;

        public Button RestartButton => restartButton;
        public Button ExitButton => exitButton;
        public Button FightButton => fightButton;
        public TMP_Text RoundText => roundText;
        public GameObject ResultGamePanel => resultGamePanel;
        public TMP_Text ResultGameText => resultGameText;
    }
}

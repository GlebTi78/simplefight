﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs
{
    internal class InfoPanelConfig : MonoBehaviour
    {
        [SerializeField] private GameObject infoPanel;
        [SerializeField] private Text logText;

        public GameObject InfoPanel => infoPanel;
        public Text LogText => logText;
    }
}

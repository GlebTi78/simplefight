﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Configs.RulesScene
{
    internal class RulesSceneUIConfig : MonoBehaviour
    {
        [SerializeField] private Button nextButton;
        [SerializeField] private Button backButton;
        [SerializeField] private Button exitButton;

        [SerializeField] private Image rulesPanelImage;
        [SerializeField] private Sprite[] pageSprites;

        public Button NextButton => nextButton;
        public Button BackButton => backButton;
        public Button ExitButton => exitButton;
        public Image RulesPanelImage => rulesPanelImage;
        public Sprite[] PageSprites => pageSprites;
    }
}

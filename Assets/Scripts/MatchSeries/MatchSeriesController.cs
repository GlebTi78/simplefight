﻿using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using Assets.Scripts.UI.GameScene;
using Assets.Scripts.Unit.GameScene;
using UnityEngine;

namespace Assets.Scripts.MatchSeries
{
    internal class MatchSeriesController : IController
    {
        private DIContainer container;
        private UIController uiController;
        private RoundController roundController;
        private PlayerController playerController;

        public MatchSeriesController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            uiController = container.Get<UIController>();
            roundController = container.Get<RoundController>();
            playerController = container.Get<PlayerController>();
        }

        public void ProcessMatchResults(MatchResultType matchResult, string resultText, bool isKnockoutEvent = false)
        {
            if (matchResult == MatchResultType.None)
            {
                Debug.LogError($"[{this.GetType().Name}] ошибка определения результата матча");  
                return;
            }

            bool isMatchSeries = new MatchSettingsHolder().IsTillDropSeries();
            if (isMatchSeries)
                TillDropMatchFinish(matchResult, resultText);
            else
                StandardMatchFinish(matchResult, resultText);
        }

        private void StandardMatchFinish(MatchResultType matchResult, string resultText)
        {
            MatchResultData data;
            data.MatchResult = matchResult;
            data.ResultText = resultText;

            uiController.MatchCompleted(data);
        }

        private void TillDropMatchFinish(MatchResultType matchResult, string resultText)
        {
            MatchResultData data;
            new MatchSettingsHolder().SaveIntermediateResult
                    (matchResult, roundController.RoundCount, playerController.GetUnitStats().Initiative);

            if (matchResult != MatchResultType.Defeat)
            {
                data.MatchResult = matchResult;
                data.ResultText = resultText;
                uiController.MatchCompleted(data);
            }
            else
            {
                data.MatchResult = matchResult;
                data.ResultText = resultText;
                uiController.MatchSeriesCompleted(data);
            }

            Debug.Log($"[{this.GetType().Name}] TillDropMatchFinish()");
        }
    }
}

﻿using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using System;

namespace Assets.Scripts.Sound.MainMenuScene
{
    internal class MainMenuSoundController : BaseSoundController, IController, IDisposable
    {
        public MainMenuSoundController(DIContainer container) : base(container)
        {
        }

        public override void Init()
        {
            
        }

        public void Dispose()
        {
            
        }

        
    }
}

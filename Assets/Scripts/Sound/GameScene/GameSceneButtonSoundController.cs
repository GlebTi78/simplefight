﻿using Assets.Scripts.Configs;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.UI;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Assets.Scripts.Sound.GameScene
{
    internal class GameSceneButtonSoundController : BaseSoundController, IController, IDisposable
    {
        private List<MouseInteractionHandlerWithEnableCheck> mouseInteractionsHandler;

        public GameSceneButtonSoundController(DIContainer container) : base(container)
        {
        }

        public override void Init()
        {
            config = Container.Get<ConfigHolder>().GetComponent<ButtonSoundControllerConfig>();

            Button[] buttons = Container.Get<ConfigHolder>().GetComponent<AllButtonsConfig>().AllButtons;
            mouseInteractionsHandler = new List<MouseInteractionHandlerWithEnableCheck>();

            foreach (var button in buttons)
            {
                if (button.TryGetComponent<MouseInteractionHandlerWithEnableCheck>
                    (out MouseInteractionHandlerWithEnableCheck component))
                {
                    mouseInteractionsHandler.Add(component);
                }
            }

            foreach (var handler in mouseInteractionsHandler)
            {
                handler.OnMouseEnter += PlayMouseEnterSound;
                handler.OnButtonClicked += PlayMouseClickSound;
            }
        }

        public void Dispose()
        {
            foreach (var handler in mouseInteractionsHandler)
            {
                handler.OnMouseEnter -= PlayMouseEnterSound;
                handler.OnButtonClicked -= PlayMouseClickSound;
            }
        }

        private void PlayMouseEnterSound()
        {
            config.AudioSource.PlayOneShot(config.MouseEnterSound);
        }

        private void PlayMouseClickSound()
        {
            config.AudioSource.PlayOneShot(config.MouseClickSound);
        }
    }
}

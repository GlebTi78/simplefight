﻿using Assets.Scripts.Configs;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using System;

namespace Assets.Scripts.Sound
{
    internal abstract class BaseSoundController : IController
    {
        protected DIContainer Container;
        protected ButtonSoundControllerConfig config;

        public BaseSoundController(DIContainer container)
        {
            this.Container = container;
        }

        public abstract void Init();
    }
}

﻿namespace Assets.Scripts.Save
{
    internal struct PlayerPrefsKeys
    {
        public const string CharacterId = "CharacterId";
        public const string BotId = "BotId";
        public const string CompetitionType = "CompetitionType"; //тип состязания

        //для состязания типа Till Drop
        public const string IsStartSeries = "IsStartSeries";
        public const string RoundsNumber = "RoundsNumber"; 
        public const string WinsNumber = "WinsNumber"; 
        public const string PlayerInitiative = "PlayerInitiative"; //оставшаяся в конце матча инициатива Player


        //Test
        public const string IsTestMatch = "IsTestMatch";
    }
}

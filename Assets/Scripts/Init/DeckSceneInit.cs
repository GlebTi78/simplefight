﻿using Assets.Scripts.Configs;
using Assets.Scripts.Interfaces;
using Assets.Scripts.UI.DeckScene;

namespace Assets.Scripts.Init
{
    internal class DeckSceneInit : EntryPoint
    {
        protected override void Create(ISceneController sceneController, ConfigHolder configHolder)
        {
            Container = new DIContainer();

            Controllers = new IController[]
            {
                sceneController,
                configHolder,
                new DeckSceneUIMainController(Container),
                new AllCardsPanelController(Container),
                new DeckPanelController(Container),
                new SelectedCardViewPanelController(Container)
               
            };
        }
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Sound;
using Assets.Scripts.UI.MatchTypeSelectScene;

namespace Assets.Scripts.Init
{
    internal class MatchTypeSelectSceneInit : EntryPoint
    {
        protected override void Create(ISceneController sceneController, ConfigHolder configHolder)
        {
            Container = new DIContainer();

            Controllers = new IController[]
            {
                sceneController,
                configHolder,
                new MatchTypeSelectSceneUIController(Container),
                new ButtonSoundController(Container)
            };
        }
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.FightModel;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using Assets.Scripts.MatchSeries;
using Assets.Scripts.Sound.GameScene;
using Assets.Scripts.Test;
using Assets.Scripts.UI.GameScene;
using Assets.Scripts.Unit.GameScene;

namespace Assets.Scripts.Init
{
    internal sealed class GameSceneInit : EntryPoint
    {
        protected override void Create(ISceneController sceneController, ConfigHolder configHolder)
        {
            Container = new DIContainer();
            
            Controllers = new IController[]
            {
                sceneController,
                configHolder,
                new UIController(Container),
                new LoadPanelController(Container),
                new MatchResultPanelController(Container),
                new CharactersViewController(Container),
                new PlayerBoostController(Container),

                new MatchSeriesController(Container),
                new MatchController(Container),
                new RoundController(Container),
                new ScoreController(Container),
                new InitiativeCheckController(Container),

                new LogPanelController(Container),
                new CentralCardPanelController(Container),
                
                new PlayerController(Container),
                new PlayerCardsController(Container),
                new PlayerStatsPanelController(Container),
                new PlayerSelectedCardPanelController(Container),

                new BotController(Container),
                new BotCardsController(Container),
                new BotStatsPanelController(Container),
                new BotSelectedCardPanelController(Container),
                new BotInitiativeBoostController(Container),

                new FightController(Container),
                new FeatureController(Container),
                new NextRoundFeaturesHolder(Container),

                new GameSceneButtonSoundController(Container)
            };
        }
    }
}

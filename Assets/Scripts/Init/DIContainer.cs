﻿using Assets.Scripts.Interfaces;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Assets.Scripts.Init
{
    internal class DIContainer
    {
        /// <summary>
        /// Зарегистрированные сервисы
        /// </summary>
        private readonly Dictionary<string, IController> services = new Dictionary<string, IController>();

        
        /// <summary>
        /// Возвращает сервис нужного типа
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Get<T>() where T : IController
        {
            string key = typeof(T).Name;
            if (!services.ContainsKey(key))
            {
                Debug.LogError($"{key} not registered with {GetType().Name}");
                throw new InvalidOperationException();
            }

            return (T)services[key];
        }

        /// <summary>
        /// Регистрация сервиса
        /// </summary>
        /// <typeparam name="T">Тип сервиса </typeparam>
        /// <param name="service">Экземпляр сервиса</param>
        public void Register<T>(T service, string name) where T : IController
        {
            //string key = typeof(T).Name;

            if (services.ContainsKey(name))
            {
                Debug.LogError($"[DIContainer] Attempted to register service of type {name} " +
                    $"which is already registered with the {GetType().Name}.");
                return;
            }

            services.Add(name, service);
            //Debug.Log($"[DIContainer] Зарегистрирован сервис, Name = {name}");
        }

        /// <summary>
        /// Убирает сервис из числа зарегистрированных
        /// </summary>
        /// <typeparam name="T">Тип сервиса.</typeparam>
        public void Unregister<T>() where T : IController
        {
            string key = typeof(T).Name;
            if (!services.ContainsKey(key))
            {
                Debug.LogError(
                    $"Attempted to unregister service of type {key} which is not registered with the {GetType().Name}.");
                return;
            }

            services.Remove(key);
        }

        public void Dispose()
        {
            services.Clear();
            Debug.Log($"[{this.GetType().Name}].Dispose");
        }
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Sound;
using Assets.Scripts.UI.InWorkingScene;

namespace Assets.Scripts.Init
{
    internal class InWorkingSceneInit : EntryPoint
    {
        protected override void Create(ISceneController sceneController, ConfigHolder configHolder)
        {
            Container = new DIContainer();

            Controllers = new IController[]
            {
                sceneController,
                configHolder,
                new InWorkingSceneUIController(Container),
                new ButtonSoundController(Container)
            };
        }
    }
}

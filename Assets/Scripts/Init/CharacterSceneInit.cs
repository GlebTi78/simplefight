﻿using Assets.Scripts.Configs;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Sound;
using Assets.Scripts.UI.CharacterScene;

namespace Assets.Scripts.Init
{
    internal class CharacterSceneInit : EntryPoint
    {
        protected override void Create(ISceneController sceneController, ConfigHolder configHolder)
        {
            Container = new DIContainer();

            Controllers = new IController[]
            {
                sceneController,
                configHolder,
                new CharacterSceneUIController(Container),
                new CharacterViewPanelController(Container),
                new ButtonSoundController(Container)
            };
        }
    }
}

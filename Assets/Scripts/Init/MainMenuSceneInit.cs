﻿using Assets.Scripts.Configs;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Sound;
using Assets.Scripts.UI.MainMenuScene;

namespace Assets.Scripts.Init
{
    internal class MainMenuSceneInit : EntryPoint
    {
        protected override void Create(ISceneController sceneController, ConfigHolder configHolder)
        {
            Container = new DIContainer();

            Controllers = new IController[]
            {
                sceneController,
                configHolder,
                new MainMenuUIController(Container),
                new ButtonSoundController(Container)
            };

        }
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.Interfaces;
using System.Collections.Generic;
using System;

namespace Assets.Scripts.Init
{
    internal abstract class EntryPoint
    {
        protected IController[] Controllers;
        protected List<IDisposable> Disposables = new List<IDisposable>();
        protected DIContainer Container;

        public List<IDisposable> Start(ISceneController sceneController, ConfigHolder configHolder)
        {
            Create(sceneController, configHolder);
            Register();
            Init();
            AddDisposables();
            FirstUpdate();

            return Disposables;
        }

        protected abstract void Create(ISceneController sceneController, ConfigHolder configHolder);

        private void Register()
        {
            foreach (var controller in Controllers)
            {
                var name = controller.GetType().Name;
                Container.Register(controller, name);
            }
        }

        private void Init()
        {
            foreach (var controller in Controllers)
            {
                controller.Init();
            }
        }

        private void AddDisposables()
        {
            foreach (var controller in Controllers)
            {
                if (controller is IDisposable)
                    Disposables.Add(controller as IDisposable);
            }
        }

        private void FirstUpdate()
        {
            foreach (var controller in Controllers)
                if (controller is IFirstUpdate)
                    (controller as IFirstUpdate).FirstUpdate();
        }
    }
}

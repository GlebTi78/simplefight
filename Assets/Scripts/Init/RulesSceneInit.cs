﻿using Assets.Scripts.Configs;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Sound;
using Assets.Scripts.UI.RulesScene;

namespace Assets.Scripts.Init
{
    internal class RulesSceneInit : EntryPoint
    {
        protected override void Create(ISceneController sceneController, ConfigHolder configHolder)
        {
            Container = new DIContainer();

            Controllers = new IController[]
            {
                sceneController,
                configHolder,
                new RulesSceneUIController(Container),
                new ButtonSoundController(Container)
            };
        }
    }
}

﻿using Assets.Scripts.Interfaces;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Timer
{
    internal class TimerController : MonoBehaviour
    {
        public void TimerStart(ITimerClient client, float duration)
        {
            StartCoroutine(UpdateTime(client, duration));
        }

        private IEnumerator UpdateTime(ITimerClient client, float duration)
        {
            yield return new WaitForSeconds(duration);
            //Debug.Log($"[{this.GetType().Name}] таймер отработал");
            client.TimerFinish();
        }
    }
}

﻿using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts.Unit
{
    internal class UnitColorHandler //не используется
    {
        private Material material;
        private GameObject go;

        public UnitColorHandler(Material material, GameObject gameObject)
        {
            this.material = material;
            go = gameObject;
        }

        public void Coloring()
        {
            float duration = 0.5f;

            var sequence = DOTween.Sequence();
            sequence.Append(material.DOColor(Color.red, duration))
                .Append(material.DOColor(Color.white, duration))
                .SetLink(go, LinkBehaviour.KillOnDestroy);
        }
    }
}

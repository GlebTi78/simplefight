﻿using Assets.Scripts.Cards.AttackingCards;
using Assets.Scripts.Cards.DefenseCards;
using Assets.Scripts.Cards;
using UnityEngine;

namespace Assets.Scripts.Unit.Characters
{
    internal sealed class Character_2 : BaseCharacter
    {
        public Character_2()
        {
            id = 2;
            name = "Рюго Тенгу";
            sprite = Resources.Load<Sprite>("Character_North_Small");
            description = "Рюго Тенгу – воин северного ветра, символ свободы. Рюго был мастером стиля Кинсэй-куки, " +
                "обладал гибкостью орла и яростью бушующего урагана. Он путешествовал по империи и защищал тех, " +
                "кто не мог защитить себя.";
            SetDeck();
        }

        protected override void SetDeck()
        {
            deck = new BaseCard[]
            {
                new Card_2013(),
                new Card_2014(),
                new Card_2015(),
                new Card_2016(),
                new Card_2017(),
                new Card_2018(),
                new Card_2019(),
                new Card_2020(),
                new Card_2021(),
                new Card_2022(),
                new Card_2023(),
                new Card_2024(),


                new Card_1021(),
                new Card_1022(),
                new Card_1023(),
                new Card_1024(),
            };
        }
    }
}

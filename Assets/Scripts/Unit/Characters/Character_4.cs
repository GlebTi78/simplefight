﻿using Assets.Scripts.Cards.AttackingCards;
using Assets.Scripts.Cards.DefenseCards;
using Assets.Scripts.Cards;
using UnityEngine;

namespace Assets.Scripts.Unit.Characters
{
    internal sealed class Character_4 : BaseCharacter
    {
        public Character_4()
        {
            id = 4;
            name = "Казума Хибики";
            sprite = Resources.Load<Sprite>("Character_East_Small");
            description = "Казума Хибики - воин восточного ветра, философ и духовный наставник. " +
                "Казума изучал древние техники, которые позволяли ему использовать " +
                "внутреннюю силу для достижения невероятных подвигов, а талант мудрого наставника " +
                "позволял ему доходчиво делиться мудростью с молодыми воинами.";
            SetDeck();
        }

        protected override void SetDeck()
        {
            deck = new BaseCard[]
            {
                new Card_1001(),
                new Card_1002(),
                new Card_1003(),
                new Card_1004(),
                new Card_1005(),
                new Card_1006(),
                new Card_1007(),
                new Card_1008(),

                new Card_2001(),
                new Card_2002(),
                new Card_2003(),
                new Card_2004(),
                new Card_2005(),
                new Card_2006(),
                new Card_2007(),
                new Card_2008(),
            };
        }
    }
}

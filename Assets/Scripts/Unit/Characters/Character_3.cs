﻿using Assets.Scripts.Cards.AttackingCards;
using Assets.Scripts.Cards.DefenseCards;
using Assets.Scripts.Cards;
using UnityEngine;

namespace Assets.Scripts.Unit.Characters
{
    internal sealed class Character_3 : BaseCharacter
    {
        public Character_3()
        {
            id = 3;
            name = "Юкико Тома";
            sprite = Resources.Load<Sprite>("Character_West_Small");
            description = "Юкико Тома – воин западного ветра, хранительница мудрости и древних традиций. " +
                "Юкико была мудрым стратегом, изучала историю и традиции боевых искусств. " +
                "Она был мастером использования хитрости и тактики в бою.";
            SetDeck();
        }

        protected override void SetDeck()
        {
            deck = new BaseCard[]
            {
                new Card_1009(),
                new Card_1010(),
                new Card_1011(),
                new Card_1012(),
                new Card_1013(),
                new Card_1014(),
                new Card_1015(),
                new Card_1016(),
                new Card_1017(),
                new Card_1018(),
                new Card_1019(),
                new Card_1020(),

                new Card_2009(),
                new Card_2010(),
                new Card_2011(),
                new Card_2012(),
            };
        }
    }
}

﻿using Assets.Scripts.Cards;
using UnityEngine;

namespace Assets.Scripts.Unit.Characters
{
    internal abstract class BaseCharacter
    {
        protected int id;
        protected string name;
        protected Sprite sprite;
        protected BaseCard[] deck;
        protected string description;

        public int Id => id;
        public string Name => name;
        public Sprite Sprite => sprite;
        public BaseCard[] Deck => deck;
        public string Description => description;

        protected abstract void SetDeck();
    }
}

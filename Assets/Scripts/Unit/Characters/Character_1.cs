﻿using Assets.Scripts.Cards.AttackingCards;
using Assets.Scripts.Cards.DefenseCards;
using Assets.Scripts.Cards;
using UnityEngine;

namespace Assets.Scripts.Unit.Characters
{
    internal sealed class Character_1 : BaseCharacter
    {
        public Character_1()
        {
            id = 1;
            name = "Хира Инари";
            sprite = Resources.Load<Sprite>("Character_South_Small");
            description = "Хира Инари – воин южного ветра, страж чести. " +
                "Она была известна своей непреклонной честностью и преданностью кодексу Сэйкен-до, была почитаема за " +
                "свою силу и бескомпромиссность. Ее меч был символом чести и обладал удивительной остротой.";
            SetDeck();
        }

        protected override void SetDeck()
        {
            deck = new BaseCard[]
            {
                new Card_1009(),
                new Card_1010(),
                new Card_1011(),
                new Card_1012(),
                new Card_1013(),
                new Card_1014(),
                new Card_1015(),
                new Card_1016(),
                new Card_1017(),
                new Card_1018(),
                new Card_1019(),
                new Card_1020(),

                new Card_2009(),
                new Card_2010(),
                new Card_2011(),
                new Card_2012(),
            };
        }
    }
}

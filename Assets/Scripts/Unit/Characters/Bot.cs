﻿using Assets.Scripts.Cards.AttackingCards;
using Assets.Scripts.Cards.DefenseCards;
using Assets.Scripts.Cards;
using UnityEngine;

namespace Assets.Scripts.Unit.Characters
{
    internal class Bot : BaseCharacter
    {
        public Bot()
        {
            id = 0;
            name = "Bot";
            sprite = Resources.Load<Sprite>("SpriteCharacter_1");
            description = "Напарник для учебного поединка.";
            SetDeck();
        }

        protected override void SetDeck()
        {
            deck = new BaseCard[]
            {
                new Card_1025(),
                new Card_1026(),
                new Card_1027(),
                new Card_1028(),
                new Card_1029(),
                new Card_1030(),
                new Card_1031(),
                new Card_1032(),

                new Card_2025(),
                new Card_2026(),
                new Card_2027(),
                new Card_2028(),
                new Card_2029(),
                new Card_2030(),
                new Card_2031(),
                new Card_2032(),
            };
        }
    }
}

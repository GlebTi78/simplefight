﻿using Assets.Scripts.Cards;
using Assets.Scripts.Cards.AttackingCards;
using Assets.Scripts.Cards.DefenseCards;
using Assets.Scripts.Save;
using Assets.Scripts.Unit.Characters;
using UnityEngine;

namespace Assets.Scripts.Unit.GameScene
{
    internal class UnitDeckInit
    {
        private BaseCard[] deck;

        public BaseCard[] GetStandartDeck()
        {
            //deck = new BaseCard[]
            //{
            //    new Card_2013(),
            //    new Card_2014(),
            //    new Card_2015(),
            //    new Card_2016(),
            //    new Card_2017(),
            //    new Card_2018(),
            //    new Card_2019(),
            //    new Card_2020(),

            //    new Card_2021(),
            //    new Card_2022(),
            //    new Card_2023(),
            //    new Card_2024(),
            //    new Card_2005(),
            //    new Card_2006(),
            //    new Card_2007(),
            //    new Card_2008(),
            //};

            deck = new Bot().Deck;

            Shuffle();
            return deck;
        }

        public BaseCard[] GetCharacterDeck()
        {
            string key = PlayerPrefsKeys.CharacterId;
            int id = PlayerPrefs.GetInt(key);

            switch (id)
            {
                case 1:
                    deck = new Character_1().Deck;
                    break;
                case 2:
                    deck = new Character_2().Deck;
                    break;
                case 3:
                    deck = new Character_3().Deck;
                    break;
                case 4:
                    deck = new Character_4().Deck;
                    break;
                default:
                    deck = new Character_1().Deck;
                    Debug.LogError($"[{this.GetType().Name}] ошибка определения персонажа");
                    break;
            }

            Shuffle();
            return deck;
        }

        private void Shuffle()
        {
            //Debug.Log($"[{this.GetType().Name}] начальный массив карт:");
            //foreach (BaseCard card in deck)
            //{
            //    Debug.Log($"[{this.GetType().Name}] {card.Id}");
            //}

            var random = new System.Random();
            for (int i = deck.Length - 1; i >= 1; i--)
            {
                int j = random.Next(i + 1);
                // обменять значения data[j] и data[i]
                var temp = deck[j];
                deck[j] = deck[i];
                deck[i] = temp;
            }

            //Debug.Log($"[{this.GetType().Name}] итоговый массив карт:");
            //foreach (BaseCard card in deck)
            //{
            //    Debug.Log($"[{this.GetType().Name}] {card.Id}");
            //}
        }
    }
}

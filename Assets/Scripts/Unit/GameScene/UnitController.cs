﻿using Assets.Scripts.Cards;
using Assets.Scripts.Cards.Features;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using Assets.Scripts.UI.GameScene;
using System;
using UnityEngine;

namespace Assets.Scripts.Unit.GameScene
{
    internal abstract class UnitController : IController, IDisposable, IFirstUpdate
    {
        protected DIContainer container;
        protected UnitManager unitManager;
        protected CardsController cardsController;
        protected UnitStatsPanelController statsPanelController;
        protected RoundController roundController;
        protected BaseCard selectedCard;
        protected UnitSelectedCardPanelController selectedCardPanelController;
        protected BaseUnitStats currentStats;
        protected int initiativeBoost;

        public UnitManager GetUnitManager() => unitManager;
        public BaseCard GetSelectedCard() => selectedCard;
        public BaseUnitStats GetUnitStats() => currentStats;
        public int GetInitiativeBoost() => initiativeBoost;

        protected UnitController(DIContainer container)
        {
            this.container = container;
        }

        public virtual void Init()
        {
            roundController = container.Get<RoundController>();
            roundController.OnRoundEnded += RoundEnded;
        }

        public void Dispose()
        {
            selectedCardPanelController.Dispose();
            cardsController.Dispose();
            roundController.OnRoundEnded -= RoundEnded;
        }

        public void FirstUpdate()
        {
            statsPanelController.UpdateBaseStatsView(currentStats);
        }

        public void CardBlocking(FeatureType featureType)
        {
            cardsController.CardBlocking(featureType);
        }

        public void GetStatsFromCard()
        {
            selectedCard.CardHasBeenUsed();
            
            currentStats.Power += selectedCard.Power;

            statsPanelController.LightEffectStart();
            statsPanelController.UpdateBaseStatsView(currentStats);
        }

        public void GetStatsFromFeature()
        {
            BaseUnitStats statsMod = selectedCard.Feature.UnitStatsMod;
            StatsMod(statsMod);
        }

        public void StatsMod(BaseUnitStats statsMod)
        {
            currentStats.Power += statsMod.Power;
            currentStats.Initiative += statsMod.Initiative;
            currentStats.Knockout += statsMod.Knockout;

            if (currentStats.Knockout < 0)
                currentStats.Knockout = 0;

            statsPanelController.UpdateBaseStatsView(currentStats);
        }

        public void UpdateInitiative(int value)
        {
            currentStats.Initiative += value;
            statsPanelController.UpdateBaseStatsView(currentStats);
        }
        
        public void KnockoutValueIncrease()
        {
            currentStats.Knockout++;
            statsPanelController.UpdateBaseStatsView(currentStats);
        }

        private void RoundEnded()
        {
            currentStats.Power = 0;
            statsPanelController.UpdateIniBoostView(0);
            statsPanelController.UpdateBaseStatsView(currentStats);
        }
    }
}

﻿namespace Assets.Scripts.Unit.GameScene
{
    internal enum CardBlockingState
    {
        None,
        OneCardBlocked,
        TwoCardsBlocked
    }
}

﻿using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.UI.GameScene;
using Assets.Scripts.Unit.GameScene.BotLogic;
using System;
using UnityEngine;

namespace Assets.Scripts.Unit.GameScene
{
    internal class BotController : UnitController, IController, IDisposable, IFirstUpdate
    {
        private BotInitiativeBoostController botInitiativeBoostController;
        
        public BotController(DIContainer container) : base(container)
        {
        }

        public override void Init()
        {
            base.Init();
            cardsController = container.Get<BotCardsController>();
            statsPanelController = container.Get<BotStatsPanelController>();
            selectedCardPanelController = container.Get<BotSelectedCardPanelController>();
            botInitiativeBoostController = container.Get<BotInitiativeBoostController>();
            unitManager = UnitManager.Bot;

            currentStats.Initiative = Resources.Load<MatchSettings>("MatchSettings").BotInitiativeStartValue;
        }

        public void SetSelectedCard()
        {
            selectedCard = new BotCardSelectionHandler().SetCard(cardsController);
            if (selectedCard == null )
            {
                Debug.LogError($"[{this.GetType().Name}] ошибка назначения карты");
                return;
            }
            
            selectedCardPanelController.ActivatePanel(selectedCard); //temporary
        }

        public void ApplyInitiativeBoost()
        {
            initiativeBoost = botInitiativeBoostController
                .SetIniBoost(selectedCard, currentStats.Initiative, currentStats.Power);
            
            UpdateInitiative(-initiativeBoost);
            statsPanelController.UpdateIniBoostView(initiativeBoost);
        }
    }
}

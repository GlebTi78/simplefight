﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using System;
using System.Linq;

namespace Assets.Scripts.Unit.GameScene
{
    internal sealed class PlayerCardsController : CardsController, IController, IDisposable
    {
        public PlayerCardsController(DIContainer container) : base(container)
        {
        }

        protected override void LoadConfig(DIContainer container)
        {
            config = container.Get<ConfigHolder>().GetComponent<PlayerCardsControllerConfig>();
            //deck = new UnitDeckInit().GetCharacterDeck().ToList();
            deck = container.Get<MatchController>().Character.Deck.ToList();
        }
    }
}

﻿using Assets.Scripts.Configs;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using Assets.Scripts.Save;
using Assets.Scripts.Unit.Characters;
using System;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Unit.GameScene
{
    internal class BotCardsController : CardsController, IController, IDisposable
    {
        public BotCardsController(DIContainer container) : base(container)
        {
        }

        protected override void LoadConfig(DIContainer container)
        {
            config = container.Get<ConfigHolder>().GetComponent<BotCardsControllerConfig>();
            
            int isTestMatch = PlayerPrefs.GetInt(PlayerPrefsKeys.IsTestMatch);
            if (isTestMatch == 1)
            {
                deck = new Bot().Deck.ToList();
            }
            else
            {
                deck = container.Get<MatchController>().Bot.Deck.ToList();
            }
        }
    }
}

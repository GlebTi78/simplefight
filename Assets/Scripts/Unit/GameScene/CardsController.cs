﻿using Assets.Scripts.Cards;
using Assets.Scripts.Cards.Features;
using Assets.Scripts.Init;
using Assets.Scripts.Match;
using Assets.Scripts.UI.GameScene;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Interfaces;
using System;
using Assets.Scripts.Test;
using Assets.Scripts.Configs.GameScene;

namespace Assets.Scripts.Unit.GameScene
{
    internal abstract class CardsController : IController, IDisposable
    {
        protected DIContainer container;
        protected CardsControllerConfig config;
        protected List<BaseCard> deck;
        
        private LogPanelController logPanelController;
        private RoundController roundController;
        private CardBlockingState cardBlockingState;
        private int currentIndex;
        private CardView cardA;
        private CardView cardB;
        private CardView cardC;
        private List<BaseCard> cardsInRound;

        public List<BaseCard> CardsInRound => cardsInRound;
        public CardBlockingState CardBlockingCurrentState => cardBlockingState;

        public CardsController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            LoadConfig(container);
            roundController = container.Get<RoundController>();
            logPanelController = container.Get<LogPanelController>();
            cardsInRound = new List<BaseCard>(4);

            cardA = config.CardA;
            cardB = config.CardB;
            cardC = config.CardC;

            UpdateCardsInPanel();

            roundController.OnNextRound += UpdateCardsInPanel;
            roundController.OnRoundEnded += UnblockCards;
            roundController.OnRoundEnded += UpdateDeck;
        }

        public void Dispose()
        {
            roundController.OnNextRound -= UpdateCardsInPanel;
            roundController.OnRoundEnded -= UnblockCards;
            roundController.OnRoundEnded -= UpdateDeck;
        }

        public void CardBlocking(FeatureType featureType)
        {
            if (featureType == FeatureType.BlockingOneCard)
            {
                cardBlockingState = CardBlockingState.OneCardBlocked;
                config.BlockingPanelCardD.SetActive(true);
            }
            else if (featureType == FeatureType.BlockingTwoCards)
            {
                cardBlockingState = CardBlockingState.TwoCardsBlocked;
                config.BlockingPanelCardC.SetActive(true);
                config.BlockingPanelCardD.SetActive(true);
            }
            else
            {
                Debug.LogError($"[{this.GetType().Name}] ошибка метода BlockingCards");
            }
        }

        protected void UnblockCards()
        {
            cardBlockingState = CardBlockingState.None;
            config.BlockingPanelCardC.SetActive(false);
            config.BlockingPanelCardD.SetActive(false);
        }

        protected abstract void LoadConfig(DIContainer container);

        private void UpdateCardsInPanel()
        {
            Shuffle();
            SetCard(cardA);
            SetCard(cardB);
            SetCard(cardC);
        }

        protected void Shuffle()
        {
            var random = new System.Random();
            for (int i = deck.Count - 1; i >= 1; i--)
            {
                int j = random.Next(i + 1);
                var temp = deck[j];
                deck[j] = deck[i];
                deck[i] = temp;
            }
        }

        private void SetCard(CardView cardView)
        {
            byte minDeckCountValue = 3;
            if (deck.Count < minDeckCountValue)
            {
                Debug.LogError($"[{this.GetType().Name}] Недостаточно карт");
                logPanelController.Handler("Ошибка: недостаточно карт для выдачи!");
                return;
            }

            if (currentIndex >= deck.Count)
                currentIndex = 0;

            bool isSetCard = false;

            while (isSetCard == false)
            {
                if (deck[currentIndex].IsUsed)
                {
                    Debug.LogError($"[{this.GetType().Name}] Ошибка алгоритма выдачи карт");
                }
                else
                {
                    cardView.SetCard(deck[currentIndex]);
                    cardsInRound.Add(deck[currentIndex]);
                    currentIndex++;
                    isSetCard = true;
                }
            }
        }

        private void UpdateDeck()
        {
            cardsInRound.Clear();
            
            if (currentIndex > 0)   //temporary
                currentIndex--;         
            

            for (int i = 0; i < deck.Count; i++)
            {
                if (deck[i].IsUsed)
                {
                    var name = deck[i].NameRu;
                    deck.RemoveAt(i);
                    
                    return;
                }
            }
        }
    }
}

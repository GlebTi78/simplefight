﻿using Assets.Scripts.Cards;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using Assets.Scripts.Save;
using Assets.Scripts.UI.GameScene;
using System;
using UnityEngine;

namespace Assets.Scripts.Unit.GameScene
{
    internal class PlayerController : UnitController, IController, IDisposable, IFirstUpdate
    {
        private PlayerBoostController initiativeControlPanelController;

        public PlayerController(DIContainer container) : base(container)
        {
        }

        public override void Init()
        {
            base.Init();
            cardsController = container.Get<PlayerCardsController>();
            statsPanelController = container.Get<PlayerStatsPanelController>();
            selectedCardPanelController = container.Get<PlayerSelectedCardPanelController>();
            initiativeControlPanelController = container.Get<PlayerBoostController>();
            unitManager = UnitManager.Player;

            //currentStats.Initiative = Resources.Load<MatchSettings>("MatchSettings").PlayerInitiativeStartValue;
            currentStats.Initiative = new MatchSettingsHolder().GetPlayerIni();
        }

        public void SetSelectedCard(BaseCard card)
        {
            selectedCard = card;
        }

        public void ApplyInitiativeBoost()
        {
            initiativeBoost = initiativeControlPanelController.InitiativeBoost;
        }
    }
}

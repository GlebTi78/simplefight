﻿using Assets.Scripts.Cards;

namespace Assets.Scripts.Unit.GameScene.BotLogic
{
    internal class UnitsCurrentData
    {
        public readonly BaseCard PlayerCard;
        public readonly BaseCard BotCard;
        public readonly int PlayerIniBoost;
        public readonly int PlayerPower;
        public readonly int BotPower;

        public UnitsCurrentData
            (BaseCard playerCard, BaseCard botCard, int playerIniBoost, int playerPower, int botPower)
        {
            PlayerCard = playerCard;
            BotCard = botCard;
            PlayerIniBoost = playerIniBoost;
            PlayerPower = playerPower;
            BotPower = botPower;
        }
    }
}

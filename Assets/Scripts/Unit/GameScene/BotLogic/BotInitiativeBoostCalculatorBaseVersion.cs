﻿using Assets.Scripts.Cards;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Unit.GameScene.BotLogic
{
    internal class BotInitiativeBoostCalculatorBaseVersion
    {
        private IniBoostCalculationData data;

        public int SetIniBoost(IniBoostCalculationData data)
        {
            this.data = data;
            byte maxPlayerCardsNumber = 3;
            
            if (IsOpposingCardTypes())
                return 0;

            //если карты Player не блокированы
            if (data.PlayerAvailableCards.Count == maxPlayerCardsNumber)
            {
                if (IsOpposingTypeOfTwoLeadingCards())
                    return 1;
            }

            int iniBoost = ApplyBasicOption();
            iniBoost = CompareInitiative(iniBoost);

            return iniBoost;
        }

        //если все доступные карты Player имеют другой тип, чем карта бота
        private bool IsOpposingCardTypes()
        {
            foreach (var playerCard in data.PlayerAvailableCards)
            {
                if (playerCard.CardActionType == data.BotCard.CardActionType)
                    return false;
            }

            Debug.Log($"[{this.GetType().Name}] у Player все доступные карты противоположного типа");
            return true;
        }

        //если две лучшие доступные карты Player имеют другой тип, чем карта бота
        private bool IsOpposingTypeOfTwoLeadingCards()
        {
            byte minNumber = 2; //минимально необходимое количество карт противоположного типа
            List<BaseCard> opposingTypeCards = new List<BaseCard>(3);
            BaseCard sameTypeCard = null; //карта того же типа, как у бота


            foreach (var playerCard in data.PlayerAvailableCards)
            {
                if (playerCard.CardActionType != data.BotCard.CardActionType)
                    opposingTypeCards.Add(playerCard);
                else
                    sameTypeCard = playerCard;
            }

            if (opposingTypeCards.Count < minNumber)
                return false;

            //сравнение ценности карт противоположного типа с ценностью карты того же типа, что и у бота
            foreach (var card in opposingTypeCards)
            {
                if (card.PrivateStats.CardValue <= sameTypeCard?.PrivateStats.CardValue)
                    return false;
            }

            Debug.Log($"[{this.GetType().Name}] у Player 2 наиболее ценные карты имеют противоположный тип, " +
                   $"чем карта бота бота");
            return true;
        }

        private int ApplyBasicOption()
        {
            int minValue = data.BotCard.PrivateStats.InitiativeMinValue;
            int maxValue = data.BotCard.PrivateStats.InitiativeMaxValue;
            Debug.Log($"[{this.GetType().Name}] minValue = {minValue}");
            Debug.Log($"[{this.GetType().Name}] maxValue = {maxValue}");

            int initiativeValue = UnityEngine.Random.Range(minValue, maxValue + 1);
            return initiativeValue;
        }

        private int CompareInitiative(int botInitiativeBoost)
        {
            if (data.PlayerIniReserve < botInitiativeBoost)
            {
                Debug.Log($"[{this.GetType().Name}] Bot уменьшил буст инициативы из-за низкого запаса инициативы у Player");
                botInitiativeBoost = data.PlayerIniReserve + 1;
            }

            return botInitiativeBoost;
        }
    }
}

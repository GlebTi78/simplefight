﻿using Assets.Scripts.Cards;
using Assets.Scripts.Configs.GameScene;
using Assets.Scripts.Init;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Match;
using Assets.Scripts.Unit.GameScene.BotLogic;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Unit.GameScene
{
    internal class BotInitiativeBoostController : IController, IDisposable
    {
        private DIContainer container;
        private RoundController roundController;
        private PlayerController playerController;
        private PlayerCardsController playerCardsController;
        private List<BaseCard> playerAvailableCards;
        private bool isFinalRound;
        private int difficultyIndex;

        public BotInitiativeBoostController(DIContainer container)
        {
            this.container = container;
        }

        public void Init()
        {
            roundController = container.Get<RoundController>();
            playerController = container.Get<PlayerController>();
            playerCardsController = container.Get<PlayerCardsController>();

            difficultyIndex = Resources.Load<BotSettings>("BotSettings").DifficultyIndex;
            //Debug.Log($"[{this.GetType().Name}] difficultyIndex = {difficultyIndex}");

            roundController.OnFinalRound += OnFinalRound;
        }

        public void Dispose()
        {
            roundController.OnFinalRound -= OnFinalRound;
        }

        public int SetIniBoost(BaseCard botCard, int botInitiativeReserve, int botPower)
        {
            int random = UnityEngine.Random.Range(1, 101);
            int iniValue = 0;

            if (difficultyIndex < random)
            {
                Debug.Log($"[{this.GetType().Name}] расчет по базовому варианту");
                IniBoostCalculationData data = GetDataForBaseVersion(botCard);
                iniValue = new BotInitiativeBoostCalculatorBaseVersion().SetIniBoost(data);
                
            }
            else
            {
                Debug.Log($"[{this.GetType().Name}] расчет по открытому варианту");
                UnitsCurrentData data = GetDataForOpenVersion(botCard, botPower);
                iniValue = new BotInitiativeBoostCalculatorOpenDataVersion().SetIniBoost(data);
            }

            iniValue = CheckIniReserve(iniValue, botInitiativeReserve);
            return iniValue;
        }
        
        private IniBoostCalculationData GetDataForBaseVersion(BaseCard botCard)
        {
            List<BaseCard> playersCardsInRound = playerCardsController.CardsInRound;
            CardBlockingState playerCardBlockingState = playerCardsController.CardBlockingCurrentState;
            
            int playerIniReserve = playerController.GetUnitStats().Initiative + playerController.GetInitiativeBoost();
            //Debug.Log($"[{this.GetType().Name}] расчет инициативы Player: {playerIniReserve}");

            switch (playerCardBlockingState)
            {
                case CardBlockingState.None:
                    playerAvailableCards = playersCardsInRound;
                    break;

                case CardBlockingState.OneCardBlocked: 
                    playerAvailableCards = new List<BaseCard> 
                    {
                        playersCardsInRound[0],
                        playersCardsInRound[1]
                    };
                    break;

                case CardBlockingState.TwoCardsBlocked:
                    playerAvailableCards = new List<BaseCard>
                    {
                        playersCardsInRound[0]
                    };
                    break;
            }

            //Debug.Log($"[{this.GetType().Name}] Доступные игроку карты:");
            //foreach (BaseCard playerCard in playerAvailableCards)
            //{
            //    Debug.Log($"[{this.GetType().Name}] {playerCard.NameRu}");
            //}

            return new IniBoostCalculationData(botCard, playerAvailableCards, playerIniReserve);
        }

        private UnitsCurrentData GetDataForOpenVersion(BaseCard botCard, int botPower)
        {
            BaseCard playerCard = playerController.GetSelectedCard();
            int playerIniBoost = playerController.GetInitiativeBoost();
            int playerPower = playerController.GetUnitStats().Power;
            
            return new UnitsCurrentData(playerCard, botCard, playerIniBoost, playerPower, botPower);
        }
        
        private int CheckIniReserve(int iniValue, int botIniReserve)
        {
            if (isFinalRound)
            {
                if (iniValue > botIniReserve)
                {
                    Debug.Log($"[{this.GetType().Name}] Недостаточный запас инициативы, корректировка");
                    iniValue = botIniReserve;
                }
            }
            else
            {
                if (iniValue >= botIniReserve)
                {
                    Debug.Log($"[{this.GetType().Name}] Недостаточный запас инициативы, корректировка");
                    iniValue = botIniReserve - 1;
                }
            }

            return iniValue;
        }

        private void OnFinalRound()
        {
            isFinalRound = true;
        }
    }
}

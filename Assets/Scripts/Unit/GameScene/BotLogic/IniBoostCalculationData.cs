﻿using Assets.Scripts.Cards;
using System.Collections.Generic;

namespace Assets.Scripts.Unit.GameScene.BotLogic
{
    internal class IniBoostCalculationData
    {
        public readonly BaseCard BotCard;
        public readonly List<BaseCard> PlayerAvailableCards;
        public readonly int PlayerIniReserve;

        public IniBoostCalculationData(BaseCard botCard, List<BaseCard> playerAvailableCards, int playerIniReserve)
        {
            BotCard = botCard;
            PlayerAvailableCards = playerAvailableCards;
            PlayerIniReserve = playerIniReserve;
        }
    }
}

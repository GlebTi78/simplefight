﻿using Assets.Scripts.Cards;
using Assets.Scripts.Configs.GameScene;
using UnityEngine;

namespace Assets.Scripts.Unit.GameScene.BotLogic
{
    internal class BotCardSelectionHandler
    {
        private CardsController botCardsController;
        private int maxIndex;
        
        public BaseCard SetCard(CardsController botCardsController)
        {
            this.botCardsController = botCardsController;
            CardBlockingState cardBlockingState = botCardsController.CardBlockingCurrentState;
            
            switch (cardBlockingState)
            {
                case CardBlockingState.None:
                    maxIndex = 2;
                    break;

                case CardBlockingState.OneCardBlocked: 
                    maxIndex = 1;
                    break;

                case CardBlockingState.TwoCardsBlocked:
                    Debug.Log($"[{this.GetType().Name}] бот выбрал единственно возможную карту");
                    return botCardsController.CardsInRound[0];

                default:
                    Debug.LogError($"[{this.GetType().Name}] ошибка метода GetCard");
                    break;
            }

            if (IsSetRandomCard())
            {
                Debug.Log($"[{this.GetType().Name}] Бот выбрал случайную карту");
                return GetRandomCard();
            }

            return GetLeadCard();
        }
        

        private bool IsSetRandomCard()
        {
            int randomCardSelectionIndex = Resources.Load<BotSettings>("BotSettings").RandomCardSelectionIndex;
            //Debug.Log($"[{this.GetType().Name}] randomCardSelectionIndex = {randomCardSelectionIndex}");
            int random = UnityEngine.Random.Range(1, 101);
            //Debug.Log($"[{this.GetType().Name}] случайное значение = {random}");
            
            if (random > randomCardSelectionIndex)
                return false;
            else
                return true;
        }

        private BaseCard GetRandomCard()
        {
            int randomIndex = UnityEngine.Random.Range(0, maxIndex + 1);
            return botCardsController.CardsInRound[randomIndex];
        }

        private BaseCard GetLeadCard()
        {
            Debug.Log($"[{this.GetType().Name}] Бот выбрал наиболее ценную карту");
            int maxCardValue = 0;
            BaseCard selectedCard = GetRandomCard();
            
            for (int i = 0; i <= maxIndex; i++)
            {
                var card = botCardsController.CardsInRound[i];
                if (card.PrivateStats.CardValue > maxCardValue)
                {
                    maxCardValue = card.PrivateStats.CardValue;
                    selectedCard = card;
                }
            }

            return selectedCard;
        }
    }
}

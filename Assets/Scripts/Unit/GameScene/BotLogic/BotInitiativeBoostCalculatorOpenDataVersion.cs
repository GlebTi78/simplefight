﻿using Assets.Scripts.Cards;
using Assets.Scripts.Cards.Features;

namespace Assets.Scripts.Unit.GameScene.BotLogic
{
    internal class BotInitiativeBoostCalculatorOpenDataVersion
    {
        private UnitsCurrentData data;

        public int SetIniBoost(UnitsCurrentData data)
        {
            this.data = data;

            if (data.PlayerCard.CardActionType != data.BotCard.CardActionType)
                return 0;

            return Calculate();
        }

        private int Calculate()
        {
            if (data.BotCard.CardActionType == ActionType.Attack)
            {
                return AttackOption();
            }
            else
            {
                return DefenseOption();
            }
        }

        private int AttackOption()
        {
            if (data.BotPower > data.PlayerPower)
            {
                return data.PlayerIniBoost + 1;
            }
            
            if (data.BotPower == data.PlayerPower)
            {
                if (data.BotCard.Prerequisite == PrerequisiteType.AttackLaunched)
                {
                    return data.PlayerIniBoost + 1;
                }
                else
                {
                    return data.PlayerIniBoost;
                }
            }

            //if (data.BotPower < data.PlayerPower)
            return data.PlayerIniBoost;
        }

        private int DefenseOption()
        {
            if (data.BotPower >= data.PlayerPower)
            {
                return data.PlayerIniBoost + 1;
            }
            else
            {
                return data.PlayerIniBoost;
            }
        }
    }
}

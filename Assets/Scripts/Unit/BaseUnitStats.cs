﻿namespace Assets.Scripts.Unit
{
    internal struct BaseUnitStats
    {
        public int Power;
        public int Initiative;
        public int Knockout;
    }
}
